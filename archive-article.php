<?php get_header(); ?>

<main class="content">

    <section class="head-inner-2 head-inner-2_contacts">
        <div class="wrapper">

            <!--    Breadcrumbs -->
            <?php if ( function_exists( 'breadcrumbs' ) ) breadcrumbs(); ?>
            <!--    End Breadcrumbs -->

            <h1 class="h2-decor wow fadeInLeftShort"><?php echo str_replace("Архивы: ", "", get_the_archive_title()); ?></h1>

        </div>
    </section>

    <section class="main-contacts">
        <div class="wrapper">
            <div class="stocks">

                <?php
                    $args = array(
                        'numberposts' => -1,
                        'post_type'   => 'article'
                    );
                    $articles = get_posts($args);
                ?>

                <?php foreach ( $articles as $article ) { ?>
                    <div class="stock-card">
                        <div class="worth-description stock-card__description wow fadeInUpShort">
                            <div class="h2 stocks__title"><?= $article -> post_title ?></div>
                            <div class="stock-card__text scroll-unvisible js-scroll">
                                <p><?= $article -> post_excerpt ?></p>
                            </div>
                            <div class="stock-card__button">
                                <a href="<?= $article -> guid ?>" class="btn btn_stock">Читать далее</a>
                            </div>
                        </div>
                        <div class="worth-img stock-card__img wow fadeInRigthShort" data-wow-delay="0.3s">
                            <div class="section-picture">
                                <div class="section-picture__img js-lazy" data-src="<?php echo get_the_post_thumbnail_url($article); ?>"></div>
                            </div>
                            <div class="decor-right decor-stock"></div>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
    </section>

</main>

<?php get_footer(); ?>
