var $win = $(window),
    $body = $('body'),
    $header = $('#header'),
    reloadBg, reloadImg;




$win.on('load', function() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        $body.addClass('ios');
    } else {
        $body.addClass('web');
    }


    $('.js-slider-main').each(function() {
        var $el = $(this);
        $el.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
            autoplay: true,
            autoplaySpeed: 5000,
            pauseOnHover: false,
            pauseOnFocus: true,
            arrows: true,
            speed: 500,
            appendArrows: '.js-append-main-arrow',
            prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-arrow-prev"></i></button>',
            nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-arrow-next"></i></button>'
        });
    });

    $body.removeClass('loaded');
        setTimeout(function() {

        $('#loader').remove()
    },3000)
    reloadImg  = function() {
        $(".js-img:not('.loaded')").each(function() {
            var $el = $(this);
            $el.attr('src', $el.data("src")).addClass('loaded');
        });
    }
    reloadImg()
    reloadBg  = function() {

        $(".js-bg:not(.loaded)").each(function() {
            var $el = $(this);
            $el.css('background-image', 'url(' + $el.data("preload") + ')').addClass('loaded');
        });  
    }
    reloadBg();
    if ($('.js-lazy').length) {
        var lazyLoadInstance = new LazyLoad({
            elements_selector: ".js-lazy"
        });

        if (lazyLoadInstance) {
            lazyLoadInstance.update();
        }
    }
    if ( $('.wow').length) {
        var wow = new WOW({
            boxClass: 'wow',
            animateClass: 'animated',

            mobile: false,
            live: true,
            scrollContainer: null,
            callback:     function(box) {
            }
        })
        wow.init();
    }





    $('.js-slider-1').each(function() {
        var $el = $(this),
            $navSlider = $el.closest('.js-slider-group').find('.js-slider-1_nav');
            var $touchMove = true,
            $touchHover =false;
            if ($el.closest('.compare__picture').length) {
                $touchMove =false;
                $touchHover = true;
            }
        $el.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            arrows: false,
            asNavFor: '.js-slider-1-1',
            pauseOnHover: $touchHover,
            swipe: $touchMove,
            autoplay: true,
            autoplaySpeed: 5000,
            responsive: [{
                breakpoint: 767,
                settings: {
                    speed: 300,
                    adaptiveHeight: true
                }
            }]
        });
        $el.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
            $navSlider.find('.js-slider-1_nav-item').eq(nextSlide).addClass('active').siblings('.js-slider-1_nav-item').removeClass('active');
        });
    })

    if ($('.js-slider-3').length > 0) {

    $('.js-slider-3').slick({
            slidesPerRow: 1,
            rows: 2,
            arrows: false,
            fade: true,
            arrows: true,
            //autoplay: true,
            autoplaySpeed: 8000,
            prevArrow: '<button type="button" class="slick-arrow slick-arrow-2 slick-prev"><i class="icon-arrow-prev"></i></button>',
            nextArrow: '<button type="button" class="slick-arrow slick-arrow-2 slick-next"><i class="icon-arrow-next"></i></button>',
            
        });
    }

    $('.compare__picture .js-slider-1').on('mouseenter touchstart', function() {

        $('.js-slider-1').slick('slickPause');
    })
    .on('mouseleave touchend', function() {
        $('.js-slider-1').slick('slickPlay');
    })
    $('.js-slider-1-1').each(function() {
        var $el = $(this),
            $navSlider = $el.closest('.js-slider-group').find('.js-slider-1_nav');
       var $touchMove = true;
            if ($el.closest('.compare__picture').length) {
                $touchMove =false;
            }
        $el.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            arrows: false,
            touchMove: $touchMove,
            swipeToSlide: $touchMove,
            asNavFor: '.js-slider-1',
            responsive: [{
                breakpoint: 767,
                settings: {
                    adaptiveHeight: true
                }
            }]
        });
        $el.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
            $navSlider.find('.js-slider-1_nav-item').eq(nextSlide).addClass('active').siblings('.js-slider-1_nav-item').removeClass('active');
        });
    })

    $('.js-slider-1_nav-item').on('click', function() {
        var $el = $(this);
        $el.addClass('active').siblings('.js-slider-1_nav-item').removeClass('active')
        $el.closest('.js-slider-group').find('.js-slider-1').slick('slickGoTo', $el.index())
    })
    .on('mouseenter', function() {
        $('.js-slider-1').slick('slickPause');
    })
    .on('mouseleave', function() {
        $('.js-slider-1').slick('slickPlay');
    })
     
    if ($('.js-slider-1_nav-item:first-child').length) {
        $('.js-slider-1_nav-item:first-child').addClass('active')
    }
    if ($('.js-slider-reviews').length) {
        $('.js-slider-reviews').slick({
            speed: 600,
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: true,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 300,
            prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-arrow-prev"></i></button>',
            nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-arrow-next"></i></button>',
            responsive: [{
                breakpoint: 767,
                settings: {
                    adaptiveHeight: true,
                    slidesToShow: 1
                }
            }]
        });
    }

        if ($('.js-slider-specialist').length) {
            $('.js-slider-specialist').slick({
                speed: 600,
                variableWidth: true,
                arrows: true,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 5000,
                speed: 300,
                prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-arrow-prev"></i></button>',
                nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-arrow-next"></i></button>'
            });
        }



    $('.js-anchor').on('click', function() {
        var $offset = $($(this).attr('data-href')).offset().top
        $('html, body').animate({
            scrollTop: $offset
        }, 500);
        return false;
    })


    $(".js-compare-img").each(function() {

        $(this).twentytwenty({
            default_offset_pct: 0.5,
            orientation: 'horizontal',
            no_overlay: true,
            move_slider_on_hover: true,
            move_with_handle_only: true,
            click_to_move: true
        });

    })




    var y_offsetWhenScrollDisabled,
        offset;
    var $topArrow = $("#arrowScroll");

    var $flagFix = false;
    var $flagFixO = false
    $win.scroll(function() {
        y_offsetWhenScrollDisabled = $win.scrollTop();
        if (y_offsetWhenScrollDisabled > 0) {
            $('#header').addClass('fixed');
            $flagFix = true;
        } else {
            if (!$body.is('.open-header')) {
            $('#header').removeClass('fixed');
            $flagFix = false;

            }
        }
        if ($topArrow.length) {
            if (y_offsetWhenScrollDisabled >= window.innerHeight) {
                $topArrow.addClass('arr-up')
            } else {
                $topArrow.removeClass('arr-up')
            }
        }
    })


    function lockScroll() {
        if ($flagFix) {
            $flagFixO = true;
        }
        offset = y_offsetWhenScrollDisabled;
        $body.addClass('scrollDisabled');
        $('html').css('margin-top', -y_offsetWhenScrollDisabled);
        if ($flagFixO) {
            setTimeout(function() {
                $('#header').addClass('fixed')
            }, 50);

        }
    }

    function unlockScroll() {
        $body.removeClass('scrollDisabled');
        $('html').css('margin-top', 0);
        $('html, body').animate({
            scrollTop: offset
        }, 0);
    }




    if ($('.js-slider-equipment-1').length) {

        $('.js-slider-equipment-1').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            infinite: false,
            arrows: false,

            focusOnSelect: true
        });
        $('.js-slider-equipment-1').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
            $('.js-slider-equipment_nav').find('.js-slider-equipment_nav-item').eq(nextSlide).addClass('active').siblings('.js-slider-equipment_nav-item').removeClass('active');

        });
    }


    if ($('.js-slide-1').length) {

        $('.js-slide-1').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            infinite: false,
            arrows: false,

            focusOnSelect: true,
            asNavFor: '.js-slide-1_text'
        });
        $('.js-slide-1').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
            $('.js-slide-1_navs').find('.js-slide-1_nav').eq(nextSlide).addClass('active').siblings('.js-slide-1_nav').removeClass('active');

        });
    }
    if ($('.js-slide-1_text').length) {

        $('.js-slide-1_text').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            infinite: false,
            arrows: false,
            adaptiveHeight: true,
            focusOnSelect: true,
            asNavFor: '.js-slide-1',
            responsive: [{
                breakpoint: 767,
                settings: {
                    speed: 300,
                    adaptiveHeight: true
                }
            }]
        });
        $('.js-slide-1').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
            $('.js-slide-1_navs').find('.js-slide-1_nav').eq(nextSlide).addClass('active').siblings('.js-slide-1_nav').removeClass('active');

        });
    }


    $('.js-slide-1_nav').on('click', function() {
        var $el = $(this);
        $el.addClass('active').siblings('.js-slide-1_nav').removeClass('active')
        $el.closest('.js-slider-group').find('.js-slide-1').slick('slickGoTo', $el.index())
    }).on('mouseenter', function() {
        $('.js-slide-1').slick('slickPause')
    }).on('mouseleave', function() {
        $('.js-slide-1').slick('slickPlay')
    })

    if ($('.js-slide-1_nav:first-child').length) {
        $('.js-slide-1_nav:first-child').addClass('active')
    }


    /* accordion packets */
    $('.js-accordion-head').on('click', function() {
        var $el = $(this);
        $el.closest('.js-accordion-wrap').toggleClass('active').find('.js-accordion-body').slideToggle(200);
        if ($el.closest('.js-accordion-wrap').is('.active')) {
            $el.closest('.js-accordion-wrap').siblings('.js-accordion-wrap').removeClass('active').find('.js-accordion-body').slideUp(200);
        }
    });
    /* accordion packets */

    $('.js-slider-equipment_nav-item').on('click', function() {
        var $el = $(this);
        $el.addClass('active').siblings('.js-slider-equipment_nav-item').removeClass('active')
        $el.closest('.js-slider-group').find('.js-slider-equipment-1').slick('slickGoTo', $el.index())
    }).on('mouseenter', function() {
        $('.js-slider-equipment-1').slick('slickPause')
    }).on('mouseleave', function() {
        $('.js-slider-equipment-1').slick('slickPlay')
    })

    if ($('.js-slider-equipment_nav-item:first-child').length) {
        $('.js-slider-equipment_nav-item:first-child').addClass('active')
    }
    // tabs
    $('.js-tab-link').on('click', function() {
        var $el = $(this),
            $href = $el.attr('data-href'),
            $parent = $el.closest('.js-tabs-wrap');
        $el.siblings('.js-tab-link').removeClass('active');
        $el.addClass('active');
        $parent.find('.js-tab').removeClass('active');
        $($href).addClass('active');
        if ($($href).find(".js-bg:not(.loaded)").length) {

            reloadImg() 
        }
        if ($($href).find(".js-img:not(.loaded)").length) {

            reloadBg()
        }
        return false;
    });
    // tabs
    $('.js-button-nav').click(function() {
        var $el = $(this);
        $el.toggleClass('active');
        $body.toggleClass('open-header');
        $('#mainNav').toggleClass('active');
        $('.js-overlay').toggleClass('overlay');
        if ($body.is('.open-header')) {
            lockScroll();
        } else {
            setTimeout(function() {
                unlockScroll();
            }, 400);
        }
        return false;
    });


    $('.js-open-dropdown').on('click', function() {
        var $flagOpen = false;
        if ($(this).closest('.js-dropdown').is('.open')) {
            $flagOpen = true;
        }
        $('.js-dropdown').removeClass('open')
        if ($flagOpen == false) {
            $(this).closest('.js-dropdown').addClass('open');
        }
        return false
    })


    $(document).on('touchstart click', function(e) {
        if ($('.js-dropdown').is('.open')) {
            if ($(e.target).closest('.js-dropdown').length > 0) {
                return;
            }
            $('.js-dropdown.open').find('.js-open-dropdown').trigger('click');

            return false;
        }

    });

    $('.js-link-nav').on('click',function() {
        if (window.innerWidth > 1199) {

        var $el = $(this);
        $el.toggleClass('active');
        $body.toggleClass('open-header');

        $el.closest('.js-header-nav').toggleClass('open')
        if ($body.is('.open-header')) {
            lockScroll();
            $('#overlayHeader').fadeIn();
        } else {
            $('#overlayHeader').fadeOut();
            setTimeout(function() {
                unlockScroll();
            }, 400);
        }  
        }
        return false;
    });
    $('.js-link-nav-service').on('click',function() {
        if (window.innerWidth > 1199) {
            var $el = $(this);
            $el.toggleClass('active');
            $body.toggleClass('open-header');
            $el.closest('.js-header-nav').toggleClass('open')
            if ($body.is('.open-header')) {
                lockScroll();
                $('#overlayHeader').fadeIn();
            } else {
                $('#overlayHeader').fadeOut();
                setTimeout(function() {
                    unlockScroll();
                }, 400);
            }
        }
        return false;
    });

    if($('.js-slider-2').length > 0) {
        $('.js-slider-2').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
            autoplay: true,
            autoplaySpeed: 5000,
            pauseOnHover: false,
            pauseOnFocus: true,
            arrows: true,
            speed: 500,
            prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-arrow-prev"></i></button>',
            nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-arrow-next"></i></button>'
        });
    }

    // navigation
    // catalog nav
    function widthMenu() {
        $('.menu-list').each(function() {
            if (window.innerWidth > 1199) {
                var widthFix1 = window.innerWidth;
                var widthFix2 = $(this).closest('.header-nav').offset().left + parseFloat($(this).css('left'));
                var widthFix3 = widthFix1 - widthFix2;
                $(this).css('width', widthFix3 + "px");
            } else {
                $(this).css('width', '');
            }
        });
    }

    widthMenu()
    $win.on('resize', function() {
        widthMenu()
    });

    $('.js-menu-wrapper > ul > li')
        // .live("mouseleave", function() {
        //     var $el = $(this);
        //     $el.closest('.js-menu-wrapper').find('.js-img-menu').removeClass('shown')

        //     $el.closest('.js-header-nav').removeClass('cat-hover');

        // })
        .live("mouseenter", function() {
            if (window.innerWidth > 1199) {
            var $el = $(this),
                $url = $el.children('a').attr('data-img');
            if (
                $el.closest('li').is('.has-children')) {
                // $el.find('ul').show()   

                $el.closest('.js-menu-wrapper').find('.has-children').find('ul').hide()
                $el.closest('.js-menu-wrapper').find('li').removeClass('hovered') 
                $el.addClass('hovered').find('ul').css('display', 'block')
                $el.closest('.js-header-nav').addClass('cat-hover')
            } else {

                $el.closest('.js-header-nav').removeClass('cat-hover')
            }
            $el.closest('.js-menu-wrapper').find('.js-img-menu').css('background-image', 'url(' + $url + ')').addClass('shown')
        }
        })

    $('.js-menu-wrapper li li')
        .live("mouseenter", function() {
            if (window.innerWidth > 1199) {

            var $el = $(this),
                $url = $el.children('a').attr('data-img');
            $el.addClass('hovered')
            $el.siblings().removeClass('hovered')    
            $el.closest('.js-menu-wrapper').find('.js-img-menu').css('background-image', 'url(' + $url + ')').addClass('shown')
            }
        })
    // $('.js-menu-wrapper li ul')
    //     .live("mouseleave", function() {

    //         $(this).closest('.js-header-nav').removeClass('cat-hover')
    //     })
    $('.js-arr-menu')
        .on("click", function() {
            var $el = $(this).closest('.has-children').find('ul');
            $el.slideToggle()
            $el.closest('li').toggleClass('opened');
            console.log(1)
        })

    $('#overlayHeader').on('click', function() {
        $('.js-link-nav').filter('.active').trigger('click');
        $('.js-link-nav-service').filter('.active').trigger('click')
    });

    // navigation



    // scroll top
    if ($topArrow.length) {
        /* arrow up*/
        $topArrow.on('mouseenter click', function() {
            var $el = $(this);
            if ($el.is('.arr-up')) {

                $('html, body').stop().animate({
                    scrollTop: 0
                }, 500);
            } else {

                $('html, body').stop().animate({
                    scrollTop: $('.content > *:first-child').offset().top + $('.content > *:first-child').outerHeight()
                }, 500);
            }
        });

    }
    // scroll top




    if ($('.js-styled').length) {
        $('.js-styled').styler({
            selectSearchLimit: 3,
            locale: 'ru'
        });
        // $('input.js-styled').on('change', function() {
        //     if ($(this).closest('.js-styled').find('label.error').length ) {


        //         if ($(this).is(':checked')) {

        //             $('.js-styled').closest('.js-styled').find('label.error').hide()
        //             }else {
        //             $('.js-styled').closest('.js-styled').find('label.error').show()
        //         }


        //     }
        // })
    };


    if ($('.js-datepicker').length) {
        var dateToday = new Date();
        $('.js-datepicker').datepicker({
            numberOfMonths: 1,
            closeText: 'Закрыть',
            prevText: '',
            minDate: dateToday,
            currentText: 'Сегодня',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
            ],
            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
                'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'
            ],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekHeader: 'Не',
            autoClose: true,
            firstDay: 1,
            dateFormat: 'dd.mm.yy',
            changeMonth: true,
            changeYear: true,
            onSelect: function(selectedDate) {

                if ($(this).is('.error')) {
                    $(this).removeClass('error').next('.error').hide()
                }
            }
        });
    };

    // $('.js-form-quiz').each(function(){
    //     var $el = $(this),
    //         $form = $el.closest('.js-tab');

    //     for (var i = 0; i < $el.find('.js-step').length -1 ; i++) {
    //         $form.find('.js-steps-nav').append('<div class="steps-quiz-nav__item js-step-nav-item"><div class="steps-quiz-nav__count">0'+(i+1)+'</div><div class="steps-quiz-nav__el"></div></div>')
    //     }
    //     $form.find('.js-step-nav-item').eq(0).addClass('active');
    // })


    $('.js-step-next').on('click', function() {
        var $form = $(this).closest('.js-tab'),
            $index = $form.find('.js-step').filter('.active').index() + 1,
            $counts = $form.find('.js-step').length
        $form.find('.js-step').filter('.active').next('.js-step').addClass('active').siblings('.js-step').removeClass('active');
        $form.find('.js-step-nav-item').eq($index).addClass('active').siblings('.js-step-nav-item').removeClass('active');
        for (var i = 0; i < $index; i++) {
            $form.find('.js-step-nav-item').eq(i).addClass('done')
        }
        if ($index > 0) {
            $form.find('.js-step-prev').removeClass('hide')
        }
        if($index == $counts -1 ) {
            $form.find('.js-steps-nav').addClass('hide');
            $form.find('.js-quiz-buttons').addClass('hide')
        }
    })

    $('.js-step-prev').on('click', function() {
        var $form = $(this).closest('.js-tab'),
            $index = $form.find('.js-step').filter('.active').index() - 1,
            $counts = $form.find('.js-step').length
        $form.find('.js-step').filter('.active').prev('.js-step').addClass('active').siblings('.js-step').removeClass('active');
        $form.find('.js-step-nav-item').eq($index).addClass('active').siblings('.js-step-nav-item').removeClass('active');
        $form.find('.js-step-nav-item').removeClass('done')
        for (var i = 0; i < $index; i++) {
            $form.find('.js-step-nav-item').eq(i).addClass('done')
        }
        if ($index == 0) {
            $form.find('.js-step-prev').addClass('hide')
        } else {
            $form.find('.js-step-prev').removeClass('hide')
        }
    })



    // validation forms
    if ($('#callbackForm').length) {

        $('#callbackForm').validate({
            rules: {
                nameCallback: {
                    required: true,
                    minlength: 2
                },
                telCallback: {
                    required: true,
                    minlenghtphone: true
                },
                acceptRules: {
                    required: true
                }
            },
            messages: {
                nameCallback: {
                    required: 'Поле должно быть заполнено',
                    minlength: 'Минимум 2 символа'
                },
                telCallback: {
                    required: 'Поле должно быть заполнено'
                },
                acceptRules: {
                    required: "Необходимо согласие"
                }
            },

            submitHandler: function(form) {
                $.fancybox.close();
                setTimeout(function() {
                    $.fancybox.open($('#successCallback'), {
                        btnTpl: {
                            smallBtn: '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small"><i class="icon-close"></i></button>'
                        }
                    });
                }, 10);
            }

        });
    }
    $('.js-form-order').each(function() {
        var $el = $(this);

        $el.validate({
            rules: {
                nameOrder: {
                    required: true,
                    minlength: 2
                },
                telOrder: {
                    required: true,
                    minlenghtphone: true
                },
                mailOrder: {
                    required: true,
                    email: true
                },

                servicesOrder: {
                    required: true
                },
                acceptRules2: {
                    required: true
                },
                dateOrder: {
                    required: true
                }
            },
            messages: {
                nameOrder: {
                    required: 'Поле должно быть заполнено',
                    minlength: 'Минимум 2 символа'
                },
                telOrder: {
                    required: 'Поле должно быть заполнено'
                },
                mailOrder: {
                    required: 'Поле должно быть заполнено',
                    email: 'ВВедите корректный email'
                },

                servicesOrder: {
                    required: 'Выберите услугу'
                },
                acceptRules2: {
                    required: "Необходимо согласие"
                },
                dateOrder: {
                    required: 'Выберите дату'
                }
            },

            validHandler: function() {

                setTimeout(function() {
                    $('.js-styled').trigger('refresh');
                }, 1)

            },

            submitHandler: function(form) {
                $.fancybox.close();
                setTimeout(function() {
                    $.fancybox.open($('#successCallback'), {
                        btnTpl: {
                            smallBtn: '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small"><i class="icon-close"></i></button>'
                        }
                    });
                }, 10);
            }

        });
    })
    $('.js-form-quiz').each(function() {
        var $el = $(this);

        $el.validate({
            rules: {
                nameQuiz: {
                    required: true,
                    minlength: 2
                },
                mailQuiz: {
                    required: true,
                    email: true
                },
                acceptRules4: {
                    required: true
                },

            },
            messages: {
                nameQuiz: {
                    required: 'Поле должно быть заполнено',
                    minlength: 'Минимум 2 символа'
                },

                mailQuiz: {
                    required: 'Поле должно быть заполнено',
                    email: 'ВВедите корректный email'
                },
                acceptRules4: {
                    required: "Необходимо согласие"
                }
            },

            validHandler: function() {

                setTimeout(function() {
                    $('.js-styled').trigger('refresh');
                }, 1)

            },
            submitHandler: function(form) {
                $.fancybox.close();
                setTimeout(function() {
                    $.fancybox.open($('#successCallback'), {
                        btnTpl: {
                            smallBtn: '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small"><i class="icon-close"></i></button>'
                        }
                    });
                }, 10);
            }

        });
    })

    if ($('#feedback').length) {
        $('#feedback').validate({
            rules: {
                nameFeedback: {
                    required: true,
                    minlength: 2
                },
                telFeedback: {
                    required: true,
                    minlenghtphone: true
                },
                mailFeedback: {
                    required: true,
                    email: true
                },

                commentFeedback: {
                    required: true
                },
                acceptRules3: {
                    required: true
                }
            },
            messages: {
                nameFeedback: {
                    required: 'Поле должно быть заполнено',
                    minlength: 'Минимум 2 символа'
                },
                telFeedback: {
                    required: 'Поле должно быть заполнено'
                },
                mailFeedback: {
                    required: 'Поле должно быть заполнено',
                    email: 'ВВедите корректный email'
                },

                commentFeedback: {
                    required: 'Поле должно быть заполнено',
                    minlength: 'Минимум 2 символа'
                },
                acceptRules3: {
                    required: "Необходимо согласие"
                }
            },

            validHandler: function() {

                setTimeout(function() {
                    $('.js-styled').trigger('refresh');
                }, 1)

            },

            submitHandler: function(form) {
                $.fancybox.close();
                setTimeout(function() {
                    $.fancybox.open($('#successCallback'), {
                        btnTpl: {
                            smallBtn: '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small"><i class="icon-close"></i></button>'
                        }
                    });
                }, 10);
            }

        });
    }





    if ($('.js-mask-tel').length) {
        $.validator.addMethod("minlenghtphone", function(value, element) {
                return value.replace(/\D+/g, '').length > 10;
            },
            "Телефон должен быть минимум 11 символов");


    }
    // validate form

$('#services').load('services.html');





  
setTimeout(function() { 
},3000)

    if (/Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor)) {
        $body.addClass('safari')
    }






// lazy loads scripts
var loadJS = function(url, implementationCode, location){
    //url is URL of external file, implementationCode is the code
    //to be called from the file, location is the location to 
    //insert the <script> element

    var scriptTag = document.createElement('script');
    scriptTag.src = url;

    scriptTag.onload = implementationCode;
    scriptTag.onreadystatechange = implementationCode;

    location.appendChild(scriptTag);
};

var codeMaskInput = function(){
    //mask tel
    if ($('.js-mask-tel').length > 0) {
        $('.js-mask-tel').inputmask("+8(999) 999 99 99");
    }
}
var codeFancybox = function(){
          if ($('.js-fancybox').length) {

        $(".js-fancybox").fancybox({
            padding: 0,
            margin: 20,
            touch: false,
            loop: true,
            animationEffect: 'zoom',
            transitionEffect: "zoom-in-out",
            speed: 350,
            transitionDuration: 300,
            smallBtn: true,
            backFocus: false,
            btnTpl: {
                smallBtn: '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small"><i class="icon-close"></i></button>'
            },
            afterShow: function() {

                $('html').css('margin-top', -offset);
            },
            afterLoad: function() {
                lockScroll();
            },
                
            afterClose: function() {

                unlockScroll();
            }
        })
    }
}

var codeScrollbar = function(){

    if ($('.js-scroll').length) {
        instance = $('.js-scroll').overlayScrollbars({
            overflowBehavior: {
                x: "visible",
                y: "scroll"
            },

            callbacks: {
                onScroll: function() {

                }
            }
        }).overlayScrollbars();
    }
}
var onloadCallback = function() {

    if ($('.js-horizontal').length) {

        function draggableScroll() {
            $('.js-horizontal').each(function() {
                var $width = 0
                $(this).find('.js-slide').each(function() {

                    var $el = $(this);
                    $width = $width + $el.outerWidth() + parseFloat($el.css('margin-left')) + parseFloat($el.css('margin-right'))
                })
                $(this).css('width', $width);
                if ($(this).is('.equipment-nav') && window.innerWidth > 1023) {
                    $(this).css('width', '');
                }
            })

            $('.js-horizontal').each(function() {

                var $carousel = $(this),
                    $view = $carousel.closest('.js-scroll-horizontal'),
                    $item = $carousel.find(".js-slide");

                var offsetScroll = $view.outerWidth() - $carousel.outerWidth()
                if (offsetScroll > 0) {
                    offsetScroll = 0
                }
                var draggable = new Draggable($carousel, {
                    type: "x",
                    //minimumMovement: 150,
                    throwProps: true,
                    bounds: {
                        maxX: 0,
                        minX: offsetScroll
                    }
                });


            })
        }
        draggableScroll()
        $win.resize(function() {

            draggableScroll()
        })

    }
}

if ($('.js-scroll').length) {
    loadJS('js/components/jquery.overlayScrollbars.js', codeScrollbar, document.body);
}
if ($('.js-fancybox').length) {
 loadJS('js/components/jquery.fancybox.min.js', codeFancybox, document.body);
}
if ($('.js-mask-tel').length > 0) {
    loadJS('js/components/maskedinput.js', codeMaskInput, document.body);
}
if ($('.js-horizontal').length) {
    loadJS('js/components/TweenMax.min.js', onloadCallback, document.body);

}
});