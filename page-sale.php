<?php
/*
Template Name: Страница Акции
*/
?>

<?php get_header(); ?>

<main class="content">
    <section class="head-inner-2 head-inner-2_contacts">
        <div class="wrapper">

            <!--    Breadcrumbs -->
            <?php if ( function_exists( 'breadcrumbs' ) ) breadcrumbs(); ?>
            <!--    End Breadcrumbs -->

            <h1 class="h2-decor wow fadeInLeftShort"><?php echo get_field('p_stock_title'); ?></h1>

        </div>
    </section>

    <section class="main-contacts">
        <div class="wrapper">
            <div class="stocks">
                <?php foreach ( get_field('p_stock_list') as $item ) { ?>

                    <div class="stock-card">
                        <div class="worth-description stock-card__description wow fadeInUpShort">
                            <div class="h2 stocks__title"><?= $item['p_stock_item_title'] ?></div>
                            <div class="stock-card__text scroll-unvisible js-scroll">
                                <p><?= $item['p_stock_item_desc'] ?></p>
                            </div>
                            <div class="stock-card__subtitle">Список услуг, участвующих в акции:</div>
                            <div class="stock-card__services js-slider-3">
                                <?php foreach ( $item['p_stock_item_posts_list'] as $itemPost ) { ?>
                                    <?php
                                        $obj = $itemPost['p_stock_item_post_item'];
                                        $price     = get_field ( 'post_service_price', $obj->ID );
                                        $old_price = get_field ( 'post_service_old_price', $obj->ID );
                                    ?>
                                        <div class="services-price__item services-price__item-stock">
                                        <div class="services-price__head">
                                            <a href="<?= $obj->guid; ?>" class="services-price__title"><?= $obj->post_title; ?></a>
                                        </div>
                                        <div class="services-price__right">
                                            <div class="services-price__counts">
                                                <span class="services-price__counts-currency services-price__new"><?= $price ?></span>
                                                <span class="services-price__old-price"><?= $old_price ?></span>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>

                            </div>
                            <div class="stock-card__info"><?= $item['p_stock_item_dop_info'] ?></div>
                            <div class="stock-card__button">
                                <span data-src="#order" class="btn btn_stock js-fancybox">записаться</span>
                            </div>
                        </div>
                        <div class="worth-img stock-card__img wow fadeInRigthShort" data-wow-delay="0.3s">
                            <div class="section-picture">
                                <div class="section-picture__img" style="background-image: url('<?= $item['p_stock_item_img'] ?>')"></div>
                            </div>
                            <div class="decor-right decor-stock"></div>
                        </div>
                    </div>

                <?php } ?>
            </div>
        </div>
    </section>

</main>

<?php get_footer(); ?>








<script>

    $('.stock-card').each(function() {

        var titles = $(this).find('.services-price__title');
        var btn = $(this).find('.btn_stock ');

        btn.click(function() {

            var list    = $('.jq-selectbox__dropdown ul');
            var options = $('select.wpcf7-select');

            list.children().remove();
            options.children().remove();

            titles.each(function () {

                var item = $(this).text();

                list.append('<li>' + item + '</li>');
                options.append('<option value="'+ item +'">' + item + '</option>');

            });

            options.trigger("refresh");

        });

    });

</script>
