<footer class="footer  wow fadeInUpShort">

    <div class="section-contacts">
        <div class="wrapper">
            <div class="contacts-main">
                <div class="contacts-main__nav">
                    <div class="logo-contacts">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" data-src="<?php echo get_field('footer_logo', 'option'); ?>" alt="Reverse" title="Reverse" class="js-lazy" />
                    </div>
                    <div class="tabs-nav contacts-nav wow fadeInLeftShort">
                        <?php $i = 1; foreach ( get_field('footer_office_list', 'option' ) as $item ) { ?>
                            <div class="tabs-nav__link js-link-map dropdown-wrapper js-dropdown" id="office<?= $i++; ?>">
                                <div class="contacts-main__address">
                                    <div class="mob-show contacts-main__mob-title"><?= $item['footer_office_item_city']; ?></div>
                                    <div class="contacts-main__details">
                                        <?= $item['footer_office_item_address']; ?>
                                        <div class="contacts-main__time"><?= $item['footer_office_item_time']; ?></div>
                                        <div class="dropdown-wrap contacts-main__phones">
                                            <div class="dropdown-head">
                                                <a href="tel:<?= $item['footer_office_item_main_phone']; ?>"><?= $item['footer_office_item_main_phone']; ?></a>
                                                <?php if ( !empty($item['footer_office_item_phone_list']) ) { ?>
                                                    <i class="icon-caret js-open-dropdown"></i>
                                                <?php } ?>
                                            </div>
                                            <?php if ( !empty($item['footer_office_item_phone_list']) ) { ?>
                                                <div class="dropdown-list">
                                                    <ul class="dropdown-phones">
                                                        <?php foreach ( $item['footer_office_item_phone_list'] as $itemPhone ) { ?>
                                                            <li class="dropdown-phones__item">
                                                                <a href="tel:<?= $itemPhone['footer_office_item_phone_item']; ?>"><?= $itemPhone['footer_office_item_phone_item']; ?></a>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="contacts-main__map wow fadeInRigthShort" data-wow-delay="0.6s" id="map"></div>

    </div>

    <div class="wrapper">

        <?php if ( get_field('footer_minimize') == false ) { ?>
        <div class="footer-top">
            <div class="footer-menu-col">
                <div class="footer-title"><?php echo get_field('header_btn_services', 'option') ?></div>
                <div class="footer-menu">
                    <ul>
                        <?php foreach ( get_field('services_nav_list', 'option') as $service_nav_item ) { ?>
                            <?php $link = $service_nav_item['services_nav_item_link']; ?>
                            <li>
                                <a href="/category/<?= $link[0]->slug ?>"><?= $service_nav_item['services_nav_item_name'] ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="footer-menu-col">
                <div class="footer-title"><?php echo get_field('header_btn_nav', 'option') ?></div>
                <div class="footer-menu footer-menu_main">
                    <ul>
                        <?php foreach ( get_field('navigation_list', 'option') as $navItem ) { ?>
                            <li>
                                <a href="<?= $navItem['navigation_item_page']; ?>"><?= $navItem['navigation_item_name']; ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <?php } ?>

        <div class="footer-bottom">
            <div class="footer-bottom__left">
                <div class="copyrights"><?php echo get_field('footer_copyrights', 'option'); ?></div>
                <div class="footer-links">
                    <div>
                        <a href="<?php echo get_field('footer_license_link', 'option'); ?>"><?php echo get_field('footer_license_name', 'option'); ?></a>
                    </div>
                    <div>
                        <a href="<?php echo get_field('footer_politik_link', 'option'); ?>"><?php echo get_field('footer_politik_name', 'option'); ?></a>
                    </div>
                </div>
            </div>
            <div class="footer-bottom__right">
                <a href="<?php echo get_field('footer_mail', 'option'); ?>" class="footer-mail"><?php echo get_field('footer_mail', 'option'); ?></a>
                <div class="footer__socials">
                    <div class="socials">
                        <div class="socials__item"> <a href="<?php echo get_field('footer_socials_facebook', 'option'); ?>" target="_blank" class="socials__link"><i class="socials__icon icon-fb"></i></a>
                        </div>
                        <div class="socials__item"> <a href="<?php echo get_field('footer_socials_vk', 'option'); ?>" target="_blank" class="socials__link"><i class="socials__icon icon-vk"></i></a>
                        </div>
                        <div class="socials__item"> <a href="<?php echo get_field('footer_socials_instagram', 'option'); ?>" target="_blank" class="socials__link"><i class="socials__icon icon-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

</div>




<!-- popups -->
<div class="window-open">
    <div class="popup callbackForm" id="callback">
        <div class="popup-content">
            <div class="popup-title">Заказать звонок</div>

            <?php echo do_shortcode('[contact-form-7 id="328" title="Заказать звонок"]'); ?>

        </div>
    </div>
    <div class="popup" id="order">
        <div class="popup-content">
            <div class="popup-title">Записаться</div>

            <?php echo do_shortcode('[contact-form-7 id="330" title="Записаться"]'); ?>

        </div>
    </div>

    <div class="popup success-popup" id="successCallback">
        <div class="popup-content">
            <div class="results-icon"> <i class="icon-succses"></i>
            </div>
            <div class="popup-results__title"><?php echo get_field('popup_thanks_title','option'); ?></div>
            <div class="popup-results__text">
                <p><?php echo get_field('popup_thanks_txt', 'option'); ?></p>
            </div>
        </div>
    </div>
</div>

<!-- popups end -->
<div id="loader" class="vertical--center">
    <div class="vertical-center__element"> <span class="preloader preloader--top"></span>
        <span class="preloader preloader--bottom"></span>
    </div>
</div>

<div class="arrow-scroll" id="arrowScroll">
    <span class="arrow-down">скролим</span>
    <span class="arrow-up">вверх</span>
</div>

<?php wp_footer(); ?>


<script defer>

    if ($('#map').length && !$('#map').is('.init')) {

        $('#map').lazyLoadGoogleMaps(
            {
                key: 'AIzaSyDq3aqFRAyd9A8pgY1sZmQ0RO2FF_2UkbU',
                libraries: false,
                signed_in: false,
                language: false,
                region: false,
                callback: function() {
                    $stylesMap = [
                        {
                            "featureType": "administrative",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#444444"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative.locality",
                            "elementType": "labels.text",
                            "stylers": [
                                {
                                    "saturation": "-100"
                                },
                                {
                                    "lightness": "-100"
                                },
                                {
                                    "gamma": "0.00"
                                },
                                {
                                    "weight": "0.01"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative.locality",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#64686c"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative.neighborhood",
                            "elementType": "labels.text",
                            "stylers": [
                                {
                                    "weight": "0.01"
                                },
                                {
                                    "lightness": "64"
                                },
                                {
                                    "gamma": "0.00"
                                },
                                {
                                    "saturation": "-100"
                                },
                                {
                                    "color": "#64686c"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "saturation": -100
                                },
                                {
                                    "lightness": 45
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#f2f2f2"
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "simplified"
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "color": "#c8d7d4"
                                },
                                {
                                    "visibility": "on"
                                }
                            ]
                        }];

                    <?php $i = 1; foreach ( get_field('footer_office_list', 'option') as $item ) { ?>
                        var center<?= $i++; ?> = {
                            lat: <?= $item['footer_office_item_coordination_lat']; ?>,
                            lng: <?= $item['footer_office_item_coordination_lng']; ?>
                        };
                    <?php } ?>

                    var marker;

                    function initMap() {
                        map = new google.maps.Map(
                            document.getElementById('map'), {
                                zoom: 11,
                                center: center1,
                                styles: $stylesMap
                            }
                        );

                        marker = new google.maps.Marker({
                            icon: '/wp-content/themes/reverse/img/icons-svg/marker.svg',
                            position: center1,
                            map: map
                        });
                    }

                    initMap();

                    $('.js-link-map').on('click', function() {
                        var $el = $(this),
                            $href = $el.attr('id');
                        $('.js-link-map').removeClass('active');
                        $el.addClass('active').closest('.contacts').find('.tab').removeClass('active');
                        $($href).addClass('active');

                        if ($href == "office1") {
                            map.setCenter(center1);
                            marker.setPosition(center1);

                        } else {
                            map.setCenter(center2);
                            marker.setPosition(center2);
                        }
                        return false;
                    });
                }
            });
    }
</script>
<script type="text/javascript" src="https://w372167.yclients.com/widgetJS" async charset="UTF-8"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-173538168-1"></script>
    
    <script async>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-173538168-1');
    </script>

    <!-- Yandex.Metrika counter -->
    <script async type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(65897986, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
    </script>
    <!--<noscript><div><img src="https://mc.yandex.ru/watch/65897986" style="position:absolute; left:-9999px;" alt="" /></div></noscript>-->
    <!-- /Yandex.Metrika counter -->

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-M98LKKX');</script>
    <!-- End Google Tag Manager -->
    
    <!-- Google Tag Manager (noscript) -->
    <!--<noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M98LKKX" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>-->
    <!-- End Google Tag Manager (noscript) -->
</body>
</html>