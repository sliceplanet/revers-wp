<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="format-detection" content="telephone=no"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1"/>
    <meta name="it-rating" content="it-rat-cd303c3f80473535b3c667d0d67a7a11" />
    <meta name="cmsmagazine" content="3f86e43372e678604d35804a67860df7" />
    <link rel="icon" type="image/x-icon" href="<?php bloginfo('template_url') ?>/favicon.ico"/>

    <title>
        <?php if ( is_archive() ) { ?>
            <?php echo str_replace("Рубрика: ", "", get_the_archive_title()); ?>
        <?php } else { ?>
            <?php the_title(); ?>
        <?php } ?>
    </title>

    <meta name="description" content=""/>
    <meta name="keywords" content=""/>

    <style>
        .loaded .arrow-scroll, .loaded .main-wrapper {
            opacity: 0;
            -webkit-filter: blur(2px);
            filter: blur(2px)
        }

        .preloader {
            display: block;
            position: relative;
            width: 1.9rem;
            height: 1.9rem;
            margin: 1px auto;
            background: #ffdfa5
        }

        .preloader--top {
            top: 0;
            -webkit-animation: preloaderCSSTop 2s infinite;
            animation: preloaderCSSTop 2s infinite
        }

        .preloader--top::after {
            position: absolute;
            content: "";
            display: block;
            width: 1.9rem;
            height: 1.9rem;
            left: 1.9rem;
            background: #ffdfa5;
            margin-left: 1px;
            -webkit-animation: preloaderCSSTopAfter 2s infinite;
            animation: preloaderCSSTopAfter 2s infinite;
            -webkit-animation-delay: .5s;
            animation-delay: .5s
        }

        .preloader--bottom {
            top: 0;
            -webkit-animation: preloaderCSSBottom 2s infinite;
            animation: preloaderCSSBottom 2s infinite;
            -webkit-animation-delay: 1.5s;
            animation-delay: 1.5s
        }

        .preloader--bottom::after {
            position: absolute;
            content: "";
            display: block;
            width: 1.9rem;
            left: 1.9rem;
            height: 1.9rem;
            background: #ffdfa5;
            margin-left: 1px;
            -webkit-animation: preloaderCSSTopAfter 2s infinite;
            animation: preloaderCSSTopAfter 2s infinite;
            -webkit-animation-delay: 1s;
            animation-delay: 1s
        }

        @-webkit-keyframes preloaderCSSTop {
            0% {
                background-color: #000
            }
            100% {
                background-color: #ffdfa5
            }
        }

        @keyframes preloaderCSSTop {
            0% {
                background-color: #000
            }
            100% {
                background-color: #ffdfa5
            }
        }

        @-webkit-keyframes preloaderCSSBottom {
            0% {
                background-color: #000
            }
            100% {
                background-color: #ffdfa5
            }
        }

        @keyframes preloaderCSSBottom {
            0% {
                background-color: #000
            }
            100% {
                background-color: #ffdfa5
            }
        }

        @-webkit-keyframes preloaderCSSTopAfter {
            0% {
                background-color: #000
            }
            100% {
                background-color: #ffdfa5
            }
        }

        @keyframes preloaderCSSTopAfter {
            0% {
                background-color: #000
            }
            100% {
                background-color: #ffdfa5
            }
        }

        @-webkit-keyframes preloaderCSSBottomAfter {
            0% {
                background-color: #000
            }
            100% {
                background-color: #ffdfa5
            }
        }

        @keyframes preloaderCSSBottomAfter {
            0% {
                background-color: #000
            }
            100% {
                background-color: #ffdfa5
            }
        }

        .vertical--center {
            width: 40px;
            height: 40px;
            position: fixed;
            left: 50%;
            top: 50%;
            margin-left: -20px;
            margin-top: -20px;
            display: none
        }

        .loaded .vertical--center {
            display: block
        }
		body{margin:0;padding:0;font-family:Effra,arial,sans-serif;background-color:#fff;height:100%;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;color:#53545d;line-height:1.4;position:relative;scroll-behavior:smooth;font-size:1.95rem!important}a,address,body,div,form,header,html,i,iframe,img,label,li,nav,p,span,ul{margin:0;padding:0;border:0;font-size:100%}header,main,nav{display:block}html{font-size:10px!important;height:100%;-webkit-text-size-adjust:none;-ms-text-size-adjust:none}input,select{font-family:Effra,arial,sans-serif}input{color:#53545d;font-family:Effra,arial,sans-serif;outline:0;border-radius:0;-moz-border-radius:0;-webkit-border-radius:0;-webkit-appearance:none}form{padding:0;margin:0}a{color:#ffdfa5;text-decoration:none;outline:0}a,div,span{outline:0!important}input[type=submit]{-webkit-appearance:none;outline:0}*{-webkit-box-sizing:border-box;box-sizing:border-box;outline:0}:after,:before{-webkit-box-sizing:border-box;box-sizing:border-box}.wrapper:after{content:".";display:block;height:0;clear:both;visibility:hidden}img{max-width:100%}@media screen and (max-width:374px){html{font-size:8px!important}}[class^=icon-]{font-family:icomoon!important;speak:none;font-style:normal;font-weight:400;font-variant:normal;text-transform:none;line-height:1;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.icon-caret:before{content:"\e902"}.icon-menu-services:before{content:"\e907"}.icon-succses:before{content:"\e911"}.main-wrapper{padding:0;min-width:320px;width:100%;position:relative;overflow:hidden;min-height:100%;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-box-pack:start;-webkit-justify-content:flex-start;-ms-flex-pack:start;justify-content:flex-start}.wrapper{min-width:320px;max-width:1414px;padding-right:5rem;padding-left:5rem;margin:0 auto;position:relative;width:100%}p{padding:1rem 0}.h2-decor{position:relative;font-weight:400;font-size:5.1rem;line-height:1.3;letter-spacing:.065em;text-transform:uppercase;color:rgba(39,42,53,.8);padding-left:8.6rem;margin-bottom:9.2rem}.h2-decor:after{content:'';display:block;width:5.6rem;background:#53545d;height:1px;position:absolute;left:0;top:2.4rem}.loaded .main-wrapper{opacity:0}.content{min-width:320px;text-align:left;width:100%;-webkit-box-ordinal-group:3;-webkit-order:2;-ms-flex-order:2;order:2;-webkit-box-flex:1;-webkit-flex-grow:1;-ms-flex-positive:1;flex-grow:1;padding-top:15rem;position:relative}.btn{background:#fff4e0;-webkit-border-radius:100rem;border-radius:100rem;border:0;font-family:Effra,arial,sans-serif;height:7.7rem;display:inline-block;font-weight:700;font-size:1.4rem;line-height:8.2rem;text-align:center;letter-spacing:.15em;text-transform:uppercase;padding:0 1.5rem;color:#242426}.checkbox-element{position:relative;display:-webkit-inline-box;display:-webkit-inline-flex;display:-ms-inline-flexbox;display:inline-flex}.checkbox-element input{-webkit-flex-shrink:0;-ms-flex-negative:0;flex-shrink:0;display:inline-block;margin-top:0;opacity:0;position:absolute;left:0;top:0}.check{position:relative;display:block;width:26px;height:26px;-webkit-box-sizing:border-box;box-sizing:border-box;-webkit-border-radius:4px;border-radius:4px;position:relative;left:0;top:-.3rem;padding:0;display:block;background:#fff;border:2px solid #ededed;font-family:icomoon}.check:before{top:50%;left:50%;position:absolute;-webkit-transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);transform:translate(-50%,-50%);font-size:1.1rem;color:#fff}.form-control{color:#000;height:70px;color:#000;background:#fff;padding-left:2.6rem;padding-right:2.6rem;font-size:1.6rem;line-height:1.9rem;line-height:1;font-family:Effra,arial,sans-serif;width:100%;border:1px solid rgba(0,0,0,.1);-webkit-border-radius:10rem;border-radius:10rem;-webkit-box-shadow:none;box-shadow:none;padding-top:1rem}::-webkit-input-placeholder{color:rgba(0,0,0,.4);line-height:1}::-moz-placeholder{color:rgba(0,0,0,.4);line-height:1}:-moz-placeholder{color:rgba(0,0,0,.4);line-height:1}:-ms-input-placeholder{color:rgba(0,0,0,.4);line-height:1}input.form-control_lg{height:70px}.box-field{margin-bottom:3.2rem}.box-field__label{font-size:1.45rem;margin-bottom:.5rem;color:#161617}.box-field_checkbox{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}.box-field_checkbox .checkbox-element{margin-right:1.2rem}.box-field_checkbox a{font-size:1.4rem;line-height:1.21;color:#161617;opacity:.5}.arrow-scroll{position:fixed;left:4rem;bottom:7rem;z-index:100;overflow:hidden;font-size:1.2rem;line-height:1.4rem;text-align:center;text-transform:uppercase;color:#000;-webkit-transform:rotate(-90deg);-ms-transform:rotate(-90deg);transform:rotate(-90deg);-webkit-transform-origin:0 0;-ms-transform-origin:0 0;transform-origin:0 0;padding:0 5px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column}.arrow-scroll .arrow-up{display:none}.arrow-down,.arrow-up{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}.arrow-down:before,.arrow-up:before{content:'';width:5.6rem;height:1px;display:block;background:#000;margin:.5rem 2.5rem 0}@media screen and (min-width:1701px){.arrow-scroll{left:-webkit-calc(50vw - 810px);left:calc(50vw - 810px)}}@media screen and (min-width:768px){.mob-show{display:none}}@media screen and (min-width:1024px){.form-control_lg{font-size:1.7rem}}@media screen and (min-width:1024px) and (max-height:700px){.arrow-scroll{bottom:0}}@media screen and (max-width:1495px){.arrow-scroll{left:2rem}}@media screen and (max-width:1365px){.wrapper{padding-left:25px;padding-right:25px}.arrow-scroll{left:25px}.h2-decor{font-size:4.1rem;line-height:4.8rem}.arrow-scroll{left:9px}}@media screen and (max-width:1199px){.h2-decor{font-size:3.7rem;line-height:4.3rem}}@media screen and (max-width:960px){.h2-decor{font-size:3.4rem;line-height:3.8rem;padding:0 0 1.8rem;margin-bottom:6rem}.h2-decor:after{top:auto;bottom:0}}@media screen and (max-width:767px){.arrow-scroll{display:none}}@media screen and (max-height:800px),(max-width:767px){.form-control{height:60px;padding-top:.5rem}}@media screen and (max-height:800px){input.form-control_lg{height:7rem}}@media screen and (max-width:374px){.wrapper{padding-left:15px;padding-right:15px}}.header{z-index:10;position:absolute;left:0;top:0;right:0;z-index:100;font-size:1.4rem;color:#000}.header .wrapper{max-width:1824px}.header .wrapper:after{display:none}.header-wrapper .wrapper{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between;-webkit-box-align:start;-webkit-align-items:flex-start;-ms-flex-align:start;align-items:flex-start}.header-main{padding-top:7.65rem;width:-webkit-calc(100% - 26.2rem);width:calc(100% - 26.2rem);display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between}.nav-logo{padding-top:7.6rem;width:15rem;padding-left:.5rem}.header-navs{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;margin-top:.1rem}.menu-wrap{margin-right:11.4rem}.menu-head{font-weight:700;font-size:1.4rem;line-height:1;letter-spacing:.03em;text-transform:uppercase;position:relative;color:#000;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}.menu-head .menu-icon{top:-.1rem;position:relative}.menu-icon{margin-right:23px;width:16px;height:12px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}.menu-icon span{width:11px;display:block;background:#000;height:1px;position:relative}.menu-icon span:before{content:'';display:block;background:#000;height:1px;width:16px;bottom:5px;position:absolute;left:0}.menu-icon span:after{content:'';display:block;background:#000;height:1px;width:6px;top:5px;position:absolute;left:0}.menu-services-icon{margin-right:2.2rem;font-size:1.2rem;position:relative;top:-.2rem}.header-nav{position:relative}.menu-list{display:none;position:absolute;left:-9.6rem;top:-7.9rem;background-color:#e6e7ec;max-height:100vh;overflow:auto}.header-navs-wrap{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}.overlay-header{position:fixed;left:0;top:0;height:100vh;right:0;background:rgba(210,212,221,.84);-webkit-backdrop-filter:blur(20px);backdrop-filter:blur(20px);z-index:1;display:none}.menu-wrapper{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between}.menu-wrapper ul{background:#fff;list-style:none;padding-bottom:4rem}.menu-wrapper li{list-style:none}.menu-wrapper li ul{background:#e6e7ec;position:absolute;padding-top:13.2rem;left:0;top:0;bottom:0;width:38.8rem;display:none;overflow:auto;-webkit-transform:translateX(53rem);-ms-transform:translateX(53rem);transform:translateX(53rem)}.menu-wrapper li ul a{font-size:1.4rem;padding:3.05rem 3.9rem 2.15rem 6.4rem}.menu-wrapper a{font-size:1.7rem;line-height:1.25;letter-spacing:.1em;text-transform:uppercase;color:rgba(39,42,53,.8);display:block;padding:3.25rem 9.6rem 2.35rem}.menu-wrapper>ul{width:53rem;position:relative;padding-top:12.4rem}.menu-wrapper i{font-size:.6rem;padding:1.4rem 1rem 1.3rem;display:none}.img-menu{position:relative;width:-webkit-calc(100% - 53rem);width:calc(100% - 53rem);background-repeat:no-repeat;background-position:50% 50%;-webkit-background-size:cover;background-size:cover;opacity:0}.menu-services:before{content:'';background:#fff5e0;-webkit-border-radius:0 0 1000px 1000px;border-radius:0 0 1000px 1000px;display:block;position:absolute;left:-5.5rem;right:-7.2rem;top:-8rem;bottom:-1.9rem;z-index:0}.menu-services .menu-head i{top:-2px;display:inline-block;position:relative}.header-right{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:start;-webkit-align-items:flex-start;-ms-flex-align:start;align-items:flex-start}.header-callbacks{margin-top:-.3rem;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}.header-callback{font-size:1.4rem;line-height:1.2;text-transform:uppercase;color:#000;margin-right:8.1rem;letter-spacing:.009em;position:relative;padding:.5rem 0}.header-callback:before{content:'';display:block;position:absolute;background:#000;width:1.3rem;height:2px;bottom:0;left:0}.header-address{white-space:nowrap;margin-bottom:2.55rem;line-height:1.21;color:#000}.header-address address{display:inline-block;font-style:normal;margin-right:1.2rem;letter-spacing:.03em}.header-links{font-size:1.7rem;margin-right:-.8rem;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between}.header-links .dropdown-wrapper+.dropdown-wrapper{margin-left:4.1rem}.header-contacts{margin-left:5.8rem}.hamburger-box{padding:.3rem 0 0 1.5rem;background:#fff4e0;-webkit-border-radius:0 0 0 6rem;border-radius:0 0 0 6rem;width:17.7rem;height:12.5rem;display:none;position:absolute;right:-25px;top:0;z-index:10;font-weight:700;font-size:1.45rem;text-transform:uppercase}.hamburger-inner{margin-right:2.2rem}.hamburger-inner,.hamburger-inner:after,.hamburger-inner:before{width:11px;height:1px;background:#000}.hamburger-inner{left:50%;top:50%;-webkit-transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);transform:translate(-50%,-50%);z-index:2}.hamburger-inner:after,.hamburger-inner:before{position:absolute;display:block;content:"";left:0;-webkit-transform-origin:50% 50%;-ms-transform-origin:50% 50%;transform-origin:50% 50%}.hamburger-inner:after{bottom:-5px;width:5px}.hamburger-inner:before{top:-5px;width:16px}.header-contacts_mob{display:none}@media screen and (min-width:1200px){.md-show{display:none}}@media screen and (max-width:1599px){.header-callback{margin-right:5rem}.header-main{padding-top:5.65rem}.nav-logo{padding-top:5.6rem}}@media screen and (max-width:1499px){.header-main{width:-webkit-calc(100% - 21rem);width:calc(100% - 21rem)}.menu-wrap{margin-right:10.4rem}.header-contacts{margin-left:4.8rem;padding-top:.3rem}}@media screen and (max-width:1439px){.header-contacts{margin-left:1.8rem}}@media screen and (max-width:1440px){.menu-wrapper>ul{width:46rem}.menu-wrapper a{padding-right:3rem}.img-menu{width:-webkit-calc(100% - 46rem);width:calc(100% - 46rem)}.menu-wrapper li ul{-webkit-transform:translateX(46rem);-ms-transform:translateX(46rem);transform:translateX(46rem);width:32rem}}@media screen and (max-width:1365px){.menu-wrap{margin-right:8.4rem}.header-main{width:-webkit-calc(100% - 19rem);width:calc(100% - 19rem)}.header-links{font-size:1.5rem;margin-right:-.2rem}.main-bl__picture{right:-8rem}}@media screen and (max-width:1250px){.menu-services:before{left:-3rem;right:-3.2rem}}@media screen and (max-width:1199px){.lg-show{display:none}.hamburger-box{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center}.header-right{width:100%;-webkit-box-pack:end;-webkit-justify-content:flex-end;-ms-flex-pack:end;justify-content:flex-end}.header-navs{position:fixed;right:100%;top:0;bottom:0;left:auto;width:100%;overflow:auto;overflow-x:hidden;background:#fff}.header-main{padding-top:5.65rem;padding-right:16rem}.nav-logo{padding-top:2.6rem}.header-wrapper{min-height:12.5rem}.header-wrapper .wrapper{position:static}.header-contacts{display:none}.img-menu,.overlay-header{display:none!important}.header-navs-wrap{-webkit-box-orient:vertical;-webkit-box-direction:reverse;-webkit-flex-direction:column-reverse;-ms-flex-direction:column-reverse;flex-direction:column-reverse}.header-contacts_mob{display:block;padding:3rem 2.6rem 3.5rem;margin:0}.header-callbacks_mob{display:none}.header-callback{font-weight:700;letter-spacing:.029em;margin-right:3rem}.header-callback:last-child{margin-right:0}.header-callback{min-width:14.5rem}.menu-services:before{display:none}.header-address{margin-bottom:3.15rem}.header-address address{display:block;margin-bottom:.8rem}.header-links{-webkit-box-pack:start;-webkit-justify-content:flex-start;-ms-flex-pack:start;justify-content:flex-start;font-size:1.7rem}.header-links .dropdown-wrapper+.dropdown-wrapper{margin:0 0 0 6rem}.header-links .dropdown-head i{margin-left:.8rem;-webkit-transform:rotate(-90deg);-ms-transform:rotate(-90deg);transform:rotate(-90deg);display:inline-block;position:relative}.header-links .dropdown-wrapper{min-width:14.5rem}.header-links .dropdown-wrapper{margin-right:3rem;letter-spacing:-.03em}.header-links .dropdown-wrapper+.dropdown-wrapper{margin:0}.header-nav{padding:4rem 0 1.8rem}.menu-services{background:#e6e7ec}.menu-head .menu-icon,.menu-services .menu-head i{display:none}.menu-list{display:block;position:static;overflow:visible;max-height:none}.menu-wrapper>ul{padding:1.3rem 0 0;width:100%;background:0 0}.menu-head{padding:0 2.9rem;font-size:1.7rem}.menu-wrapper a{font-size:1.4rem;padding:2rem 4.2rem 1.4rem 1.7rem}.menu-wrap .menu-list{background:0 0}.menu-wrapper i{display:inline-block;-webkit-transform:rotate(-90deg);-ms-transform:rotate(-90deg);transform:rotate(-90deg);position:absolute;right:1.4rem;top:1.2em;z-index:10}.menu-wrapper li{position:relative}.menu-wrapper li li{margin-left:0}.menu-wrapper>ul>li>a{padding-left:3rem}.menu-wrapper li.has-children{-webkit-border-radius:20px 0 0 20px;border-radius:20px 0 0 20px}.menu-wrapper li ul{background:0 0}.menu-wrapper li ul{position:static;-webkit-transform:none;-ms-transform:none;transform:none;display:none;padding:0;width:100%;margin-top:-.9rem}.menu-wrapper li ul a{font-size:1.22rem;padding:2.1rem 3.9rem 1.45rem 3.1rem}}@media screen and (max-width:767px){.hamburger-box{right:0}.logo{max-width:80px;display:block}}@media screen and (max-width:715px){.header-callbacks{display:none}.header-callbacks_mob{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;margin-bottom:3.2rem}.nav-logo{padding-left:11%}.header-callback:before{display:none}}@media screen and (max-width:374px){.header-contacts_mob{padding-left:1.5rem;padding-right:1.5rem}.header-callback,.header-links .dropdown-wrapper{min-width:13.5rem;font-size:1.4rem;margin-right:2.5rem}.header-links .dropdown-wrapper{font-size:1.5rem}}.home .content{padding-top:0}.section-main{position:relative;margin-bottom:8.6rem}.main-title{font-size:2.45rem;line-height:2.9rem;letter-spacing:.1em;text-transform:uppercase;color:#000}.h1-main{font-weight:700;font-size:18.94vw;line-height:1.2;letter-spacing:.03em;text-transform:uppercase;color:#fff;text-shadow:0 30px 45px rgba(0,0,0,.05)}.main-text{position:absolute;top:43rem;left:50%;-webkit-transform:translateX(-58%);-ms-transform:translateX(-58%);transform:translateX(-58%);z-index:2;text-align:center}.main-subtitle{font-size:1.7rem;line-height:1.18;letter-spacing:.1em;text-transform:uppercase;color:#000;margin-top:12px}.arrow-1{font-weight:700;font-size:1.1rem;line-height:1.18;text-align:center;letter-spacing:.1em;text-transform:uppercase;color:#000;background:#fff;-webkit-border-radius:50%;border-radius:50%;width:16.9rem;height:16.9rem;padding-top:.6rem;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;position:relative}.arrow-1:after{content:'';display:block;width:5.6rem;background:#000;height:1px;position:absolute;left:100%;margin-left:-3.05rem;top:50%}.main-bl{z-index:1}.main-bl__head{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;position:relative;z-index:3}.main-bl__heading{padding-top:1.3rem}.main-bl__picture{position:absolute;right:6rem;min-width:50rem;bottom:0;z-index:2;mix-blend-mode:darken;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-box-pack:end;-webkit-justify-content:flex-end;-ms-flex-pack:end;justify-content:flex-end}.main-bl__picture img{max-height:81vh;vertical-align:top}.slider-main{overflow:hidden}.slider-main .wrapper{max-width:1290px}.slider-main__decor{-webkit-border-radius:0 0 0 600px;border-radius:0 0 0 600px;bottom:7.1rem;position:absolute;left:-.7rem;top:0;z-index:1;width:5000px}.slider-main-decor{background:#fff4e0;-webkit-border-radius:0 0 0 200px;border-radius:0 0 0 200px;bottom:0;position:absolute;left:25%;top:0;width:5000px;z-index:0}.slider-main__card{min-height:100vh;position:relative;padding-top:21.45rem;z-index:2}.slider-main__item{vertical-align:top}.slider-main .arrow-1{margin-left:3.8rem}.slider-main.sliders-arrow{padding-bottom:0}.section-worth .h2-decor{margin-bottom:2.8rem}.sliders-arrow{padding-bottom:6rem;position:relative}.contacts-nav .tabs-nav__link *{color:rgba(39,42,53,.5)}.dropdown-head{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;position:relative;z-index:11}.dropdown-head a{color:inherit}.dropdown-head i{font-size:.6rem;display:inline-block;padding:5px 0 5px .5rem;-webkit-flex-shrink:0;-ms-flex-negative:0;flex-shrink:0;margin-left:.9rem;margin-top:-.5rem}.dropdown-wrap{position:relative}.dropdown-wrapper{position:relative;z-index:0}.dropdown-list{opacity:0;z-index:10;-webkit-transform:translateY(15px);-ms-transform:translateY(15px);transform:translateY(15px);visibility:hidden;-webkit-border-radius:6rem;border-radius:6rem;position:absolute;background:#fff4e0;-webkit-box-shadow:0 30px 45px rgba(0,0,0,.1);box-shadow:0 30px 45px rgba(0,0,0,.1);top:-4.8rem;right:-3.1rem;padding-top:9rem;width:29.9rem;overflow:hidden;z-index:3}.dropdown-phones{list-style:none;padding:.5rem 0 3.3rem}.dropdown-phones__item{list-style:none}.dropdown-phones__item a{font-size:1.4rem;line-height:1.7rem;padding:1.95rem 4rem 1.7rem 4.8rem;display:block;color:#000}.loaded .main-text{opacity:0;-webkit-transform:translateX(-30px);-ms-transform:translateX(-30px);transform:translateX(-30px)}.loaded .main-bl__picture{opacity:0;-webkit-transform:translateX(30px);-ms-transform:translateX(30px);transform:translateX(30px)}.loaded .main-bl__head{opacity:0;-webkit-transform:translateX(-30px);-ms-transform:translateX(-30px);transform:translateX(-30px)}.loaded .arrow-scroll{opacity:0;-webkit-transform:translateY(15px) rotate(-90deg);-ms-transform:translateY(15px) rotate(-90deg);transform:translateY(15px) rotate(-90deg)}.main-bl__arrow,.main-bl__heading{opacity:0;-webkit-transform:translateX(-30px);-ms-transform:translateX(-30px);transform:translateX(-30px)}.main-bl__picture img{-webkit-transform:translateX(30px);-ms-transform:translateX(30px);transform:translateX(30px);opacity:0;display:inline-block;vertical-align:top}@media screen and (min-width:1901px){.h1-main{font-size:36rem}}@media screen and (min-width:1024px) and (max-height:700px){.h1-main{font-size:37.94vh}.slider-main__card{min-height:60rem}.main-bl__picture img{max-height:457px}}@media screen and (max-width:1700px){.main-text{-webkit-transform:translateX(-50%);-ms-transform:translateX(-50%);transform:translateX(-50%)}}@media screen and (max-width:1199px){.dropdown-phones__item{margin-bottom:1.4rem}.dropdown-list{padding-top:5rem;-webkit-border-radius:20px;border-radius:20px;left:-2.5rem;top:-2rem;width:auto;min-width:-webkit-calc(100% + 3.5rem);min-width:calc(100% + 3.5rem)}.dropdown-phones__item a{padding:1.05rem 4rem .8rem 3.1rem}.dropdown-phones{padding:.5rem 0 .3rem}}@media screen and (min-width:1024px) and (max-height:800px){.slider-main__card{padding-top:18rem}.main-text{top:37rem}.slider-main__decor{-webkit-border-radius:0 0 0 400px;border-radius:0 0 0 400px}.slider-main-decor{-webkit-border-radius:0 0 0 150px;border-radius:0 0 0 150px;left:19%}}@media screen and (max-width:960px){.main-bl__picture{min-width:45rem;text-align:right;right:-1rem}.main-bl__picture img{max-height:77vh}.slider-main__card{padding-top:19.75rem}.main-bl__head{display:block;max-width:60%}.main-bl__heading{padding-top:0}.slider-main .arrow-1{margin:0}.arrow-1{width:11.2rem;height:11.2rem}.arrow-1:after{margin-left:-1.25rem}.main-bl__arrow{margin-top:3rem}}@media screen and (max-width:767px){.main-title{color:#53545d}.main-bl__head{max-width:345rem}.main-bl__arrow{margin-top:4.3rem}.arrow-1{font-size:1rem}.arrow-1:after{width:3.4rem;margin-left:-.6rem}.main-bl__picture{min-width:38rem;right:-10.5rem;top:26rem}.main-bl__picture img{max-height:100%}.slider-main__card{min-height:73rem}.main-text{-webkit-transform:none;-ms-transform:none;transform:none;left:0;padding-left:8px;padding-right:8px;text-align:left;top:51.2rem;max-width:42rem}.h1-main{font-size:14.4rem;line-height:13rem;word-wrap:break-word;letter-spacing:inherit}.slider-main__item{padding-bottom:4.5rem}.slider-main__decor{-webkit-border-radius:0 0 0 200px;border-radius:0 0 0 200px;left:-1.7rem}.section-main{margin-bottom:4.1rem}.slider-main-decor{left:-10.5%;-webkit-border-radius:0 0 0 200px;border-radius:0 0 0 200px}.sliders-arrow{padding-bottom:5rem}}@media screen and (max-width:374px){.arrow-1{font-size:1.1rem}.h1-main{font-size:15.4rem}}.window-open{position:absolute;left:0;top:0;right:0;-webkit-transform:translate(0,-200%);-ms-transform:translate(0,-200%);transform:translate(0,-200%)}
		footer {-webkit-box-ordinal-group: 4;-webkit-order: 3; -ms-flex-order: 3; order: 3;}
		.breadcrumb{font-size:1.45rem;line-height:1.25;color:#272a35;margin-bottom:4rem;list-style-type:none}.breadcrumb a{color:inherit}.breadcrumb li{display:inline-block}.breadcrumb-item:after{content:"/";margin:0 5px}.services-price__item{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between}.services-price__head{font-size:1.8rem;line-height:2.2rem;color:#000;width:-webkit-calc(100% - 473px);width:calc(100% - 473px);max-width:67.2rem;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;display:flex;align-items:center;-webkit-flex-shrink:10;-ms-flex-negative:10;flex-shrink:10;-webkit-box-flex:0;-webkit-flex-grow:0;-ms-flex-positive:0;flex-grow:0}.services-price__title{color:#000;font-size:1.9rem;display:block;white-space:nowrap;overflow:hidden;-o-text-overflow:ellipsis;text-overflow:ellipsis;padding-top:.7rem}.services-price__counts{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:baseline;-webkit-align-items:baseline;-ms-flex-align:baseline;align-items:baseline;-webkit-flex-shrink:0;-ms-flex-negative:0;flex-shrink:0;font-size:1.8rem;line-height:3rem;padding-top:.7rem}.services-price__right{margin-left:3rem;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between;width:440px;-webkit-flex-shrink:0;-ms-flex-negative:0;flex-shrink:0}.services-price__counts-currency{font-weight:700;letter-spacing:.15em}.services-price__new{color:rgba(244,54,40,.8)}.services-price__old-price{letter-spacing:.13em;color:rgba(0,0,0,.3);font-size:1.5rem;margin-left:1.1rem;position:relative}.services-price__old-price:before{content:'';display:block;left:0;right:0;top:40%;position:absolute;height:1px;background:rgba(0,0,0,.3)}.services-price__item+.services-price__item{margin-top:.95rem}.loaded .breadcrumb{opacity:0;-webkit-transform:translateX(-30px);-ms-transform:translateX(-30px);transform:translateX(-30px)}.head-inner-2{padding-top:19.4rem;z-index:0;position:relative}.head-inner-2 .wrapper{z-index:2}.head-inner-2 .h2-decor{margin-top:7.5rem;margin-bottom:7.2rem}.head-inner-2_contacts{position:relative;z-index:1}.head-inner-2_contacts .h2-decor{margin-bottom:4.2rem}.main-contacts{position:relative;z-index:0}.stocks{margin-top:11rem}.stock-card{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:start;-webkit-align-items:flex-start;-ms-flex-align:start;align-items:flex-start}.stock-card+.stock-card{margin-top:22.5rem}.stock-card__subtitle{font-size:2.2rem;line-height:1.25;text-transform:uppercase;letter-spacing:.04em;margin-top:4.8rem;margin-right:60px}.stock-card__button{margin-top:3rem}.stock-card__info{font-size:2.2rem;line-height:1.3;margin-top:-.5rem}.stock-card:nth-child(even){-webkit-box-orient:horizontal;-webkit-box-direction:reverse;-webkit-flex-direction:row-reverse;-ms-flex-direction:row-reverse;flex-direction:row-reverse}.stock-card:nth-child(even) .stock-card__img{padding-left:0}.stock-card:nth-child(even) .decor-stock{left:auto;right:24rem;-webkit-border-radius:0 100rem 100rem 0;border-radius:0 100rem 100rem 0}.stock-card:nth-child(even) .stock-card__services{max-width:none}.section-picture{position:relative;z-index:1}.btn_stock{min-width:25.7rem}.stock-card__img{padding-left:8rem}.stock-card__description{display:block}.stock-card__description p:last-child{padding-bottom:3rem}.stocks__title{margin-bottom:2.7rem}.stock-card__text{margin-left:0;letter-spacing:-.01em;line-height:1.75;padding-right:5.5rem}.stock-card__text{max-height:14rem}.decor-stock{background:#f2f3f5;-webkit-border-radius:100rem 0 0 100rem;border-radius:100rem 0 0 100rem;left:22rem;top:-7.5rem;bottom:-7.5rem}.services-price__item-stock{background:#fff;-webkit-box-shadow:0 25px 40px rgba(0,0,0,.1);box-shadow:0 25px 40px rgba(0,0,0,.1);-webkit-border-radius:50px;border-radius:50px;padding:1rem 3rem 1rem 4.5rem;min-height:8rem;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;display:-webkit-box!important;display:-webkit-flex!important;display:-ms-flexbox!important;display:flex!important}.services-price__item-stock .services-price__head{width:-webkit-calc(100% - 100px);width:calc(100% - 100px);padding-right:4.5rem}.services-price__item-stock .services-price__title{white-space:normal;max-height:51px;overflow:hidden;font-size:2rem;letter-spacing:.085em}.services-price__item-stock .services-price__right{width:auto;-webkit-flex-shrink:0;-ms-flex-negative:0;flex-shrink:0}.services-price__item-stock+.services-price__item-stock{margin-top:1.5rem}.stock-card__services{max-width:82.8rem;margin:0 -4rem;position:relative}@media screen and (max-width:1366px){.stock-card+.stock-card{margin-top:17.5rem}.stocks{margin-top:7rem}}@media screen and (max-width:1320px){.stock-card__img{padding-left:0}}@media screen and (max-width:1320px) and (min-width:961px){.stock-card__description{width:-webkit-calc(100% - 29.7rem);width:calc(100% - 29.7rem)}.stock-card__img{width:29.7rem}.stock-card:nth-child(even) .stock-card__description{padding-left:6rem}}@media screen and (max-width:1200px){.head-inner-2{padding-top:14.4rem}}@media screen and (max-width:1024px){.head-inner-2 .h2-decor{margin-top:3.5rem;margin-bottom:3.2rem}.main-contacts{z-index:1}}@media screen and (max-width:1023px){.services-price__item{display:block}.services-price__right{margin:1rem 0 0;width:100%}.services-price__head{width:100%}.services-price__item{border-bottom:1px solid rgba(0,0,0,.1);padding-bottom:2rem}.services-price__item:last-child{border-bottom:0;padding-bottom:0}.services-price__item+.services-price__item{padding-top:2rem}.services-price__title{white-space:normal}}@media screen and (max-width:960px){.stock-card__description{width:100%}.stock-card__text{max-height:none}.stock-card,.stock-card:nth-child(even){-webkit-box-orient:vertical;-webkit-box-direction:reverse;-webkit-flex-direction:column-reverse;-ms-flex-direction:column-reverse;flex-direction:column-reverse}.stock-card+.stock-card{margin-top:8.5rem}.stocks__title{margin-bottom:1.2rem}.stock-card__text{font-size:1.7rem}.stocks{margin-top:4rem}.decor-stock{left:22rem;top:-1.5rem;bottom:-1.5rem}.stock-card__img{padding-left:0}.stock-card__description{margin-top:4rem}.stock-card__subtitle{margin-top:.8rem}.stock-card__img{margin:0 auto}.services-price__counts{padding-top:.1rem}}@media screen and (max-width:767px){.head-inner-2{padding-top:12.4rem}.head-inner-2 .h2-decor{margin-top:.5rem;margin-bottom:3.2rem}.services-price__item-stock{padding-left:3rem;padding-right:3rem}.services-price__item-stock .services-price__right{margin:.4rem 0 0;width:100%}.services-price__item-stock{display:block!important}.services-price__item-stock .services-price__head{width:100%;padding-right:0}}@media screen and (max-width:479px){.services-price__right{display:block}}@media screen and (min-width:768px){.services-price__item-stock{display:-webkit-box!important;display:-webkit-flex!important;display:-ms-flexbox!important;display:flex!important}}@-webkit-keyframes fadeInLeftShort{from{opacity:0;-webkit-transform:translate3d(-15px,0,0);transform:translate3d(-15px,0,0)}to{opacity:1;-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}}@keyframes fadeInLeftShort{from{opacity:0;-webkit-transform:translate3d(-15px,0,0);transform:translate3d(-15px,0,0)}to{opacity:1;-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}}.fadeInLeftShort{-webkit-animation-name:fadeInLeftShort;animation-name:fadeInLeftShort}@-webkit-keyframes fadeInRigthShort{from{opacity:0;-webkit-transform:translate3d(30px,0,0);transform:translate3d(30px,0,0)}to{opacity:1;-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}}@keyframes fadeInRigthShort{from{opacity:0;-webkit-transform:translate3d(30px,0,0);transform:translate3d(30px,0,0)}to{opacity:1;-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}}.fadeInRigthShort{-webkit-animation-name:fadeInRigthShort;animation-name:fadeInRigthShort}@-webkit-keyframes fadeInUpShort{from{opacity:0;-webkit-transform:translate3d(0,15px,0);transform:translate3d(0,15px,0)}to{opacity:1;-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}}@keyframes fadeInUpShort{from{opacity:0;-webkit-transform:translate3d(0,15px,0);transform:translate3d(0,15px,0)}to{opacity:1;-webkit-transform:translate3d(0,0,0);transform:translate3d(0,0,0)}}.fadeInUpShort{-webkit-animation-name:fadeInUpShort;animation-name:fadeInUpShort}
    </style>

    <?php wp_head(); ?>

    



</head>

<body class="home loaded">

<div class="main-wrapper">

    <header class="header" id="header">

        <div class="header-wrapper">

            <div class="wrapper">

                <div class="nav-logo">
                    <a <?php if( !is_front_page() ) { echo 'href="/"'; } ?> class="logo">
                        <img src="<?php echo get_field('header_logo', 'option') ?>"
                             alt="Reverse"
                             title="Reverse" />
                    </a>
                </div>

                <div class="header-main">

                    <div class="header-navs" id="mainNav">
                        <div class="header-contacts header-contacts_mob">
                            <div class="header-address">
                                <address class="header-address__addres"><?php echo get_field('header_adress', 'option') ?></address>
                                <span class="hours-work"><?php echo get_field('header_time', 'option') ?></span>
                            </div>
                            <div class="header-callbacks header-callbacks_mob">
                                <div class="header-callback js-fancybox" data-src="#callback"><?php echo get_field('header_btn_order_call', 'option') ?></div>
                                <div class="header-callback js-fancybox" data-src="#order"><?php echo get_field('header_btn_appoiment', 'option') ?></div>
                            </div>
                            <div class="header-links">
                                <div class="dropdown-wrap dropdown-wrapper js-dropdown">
                                    <div class="dropdown-head js-open-dropdown"><?php echo get_field('header_btn_messangers', 'option') ?>
                                        <i class="icon-caret"></i>
                                    </div>

                                    <div class="dropdown-list">
                                        <ul class="dropdown-phones">
                                            <li class="dropdown-phones__item">
                                                <a href="whatsapp/send?phone=<?php echo get_field('header_messanger_whatsapp', 'option'); ?>">Whatsapp</a>
                                            </li>
                                            <li class="dropdown-phones__item">
                                                <a href="viber://chat?number=<?php echo get_field('header_messanger_viber', 'option'); ?>">Viber</a>
                                            </li>
                                            <li class="dropdown-phones__item">
                                                <a href="tg://resolve?domain=<?php echo get_field('header_messanger_telegram', 'option'); ?>">Telegram</a>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                                <div class="dropdown-wrap dropdown-wrapper js-dropdown">

                                    <div class="dropdown-head">

                                        <a href="tel:<?php echo get_field('header_phone_main', 'option'); ?>"><?php echo get_field('header_phone_main', 'option'); ?></a>

                                        <?php if ( !empty(get_field('header_phone_list', 'option')) ) { ?>
                                            <i class="icon-caret js-open-dropdown"></i>
                                        <?php } ?>
                                    </div>

                                    <?php if ( !empty(get_field('header_phone_list', 'option')) ) { ?>
                                        <div class="dropdown-list">
                                            <ul class="dropdown-phones">
                                                <?php foreach ( get_field('header_phone_list', 'option') as $phone ) { ?>
                                                    <li class="dropdown-phones__item">
                                                        <a href="tel:<?= $phone['header_phone_item']; ?>"><?= $phone['header_phone_item']; ?></a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>

                        <div class="header-navs-wrap">
                            <div class="header-nav menu-wrap js-header-nav">

                                <div class="menu-head js-link-nav">
                                    <span class="menu-icon">
                                        <span></span>
                                    </span>

                                    <span class="lg-show"><?php echo get_field('header_btn_nav', 'option') ?></span>

                                    <span class="md-show">Другое</span>
                                </div>

                                <div class="menu-list js-menu-list">
                                    <nav class="menu-wrapper js-menu-wrapper">
                                        <ul>
                                            <?php foreach ( get_field('navigation_list', 'option') as $navItem ) { ?>
                                                <li>
                                                    <a href="<?= $navItem['navigation_item_page']; ?>" data-img="<?= $navItem['navigation_item_img']; ?>"><?= $navItem['navigation_item_name']; ?></a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                        <div class="img-menu js-img-menu"></div>
                                    </nav>
                                </div>

                            </div>

                            <div class="header-nav menu-services js-header-nav">
                                <div class="menu-head js-link-nav-service">
                                    <i class="icon-menu-services menu-services-icon"></i>
                                    <span><?php echo get_field('header_btn_services', 'option') ?></span>
                                </div>

                                <div class="menu-list" id="mainNavService">
                                    <nav class="menu-wrapper js-menu-wrapper">
                                        <ul>
                                            <?php foreach ( get_field('services_nav_list', 'option') as $service_nav_item ) { ?>
                                                <?php $link = $service_nav_item['services_nav_item_link']; ?>
                                                <li class="<?php if ( !empty($service_nav_item['services_nav_item_sub_list']) ) { echo 'has-children'; } ?>">
                                                    <a href="/category/<?= $link[0]->slug ?>" data-img="<?= $service_nav_item['services_nav_item_img'] ?>"><?= $service_nav_item['services_nav_item_name'] ?></a>
                                                    <i class="icon-caret js-arr-menu"></i>

                                                    <?php if ( !empty( $service_nav_item['services_nav_item_sub_list'] ) ) { ?>

                                                    <ul>
                                                        <?php foreach ($service_nav_item['services_nav_item_sub_list'] as $service_subnav_item) { ?>
                                                            <?php $sublink = $service_subnav_item['services_nav_item_sub_item_link'];
//                                                            var_dump($sublink);
                                                            ?>
                                                            <li>
                                                                <a href="<?= $sublink -> guid ?>"
                                                                   data-img="<?php
                                                                   if ( !empty ( $service_subnav_item['services_nav_item_sub_item_img'] ) ) {
                                                                       echo $service_subnav_item['services_nav_item_sub_item_img'];
                                                                   } else {
                                                                       echo get_the_post_thumbnail_url($sublink);
                                                                   }
                                                                   ?>">
                                                                    <?php if ( !empty ($service_subnav_item['services_nav_item_sub_item_name']) ) { ?>
                                                                        <?= $service_subnav_item['services_nav_item_sub_item_name'] ?>
                                                                    <?php } else { ?>
                                                                        <?= $sublink -> post_title ?>
                                                                    <?php } ?>
                                                                </a>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>

                                                    <?php } ?>

                                                </li>
                                            <?php } ?>
                                        </ul>

                                        <div class="img-menu js-img-menu"></div>

                                    </nav>
                                </div>
                            </div>

                            <div class="overlay-header" id="overlayHeader"></div>

                        </div>
                    </div>

                    <div class="header-right">
                        <div class="header-callbacks">
                            <div class="header-callback js-fancybox" data-src="#callback"><?php echo get_field('header_btn_order_call', 'option') ?></div>
                            <div class="header-callback js-fancybox" data-src="#order"><?php echo get_field('header_btn_appoiment', 'option') ?></div>
                        </div>
                        <div class="header-contacts">
                            <div class="header-address">
                                <address class="header-address__addres"><?php echo get_field('header_adress', 'option'); ?></address>
                                <span class="hours-work"><?php echo get_field('header_time', 'option'); ?></span>
                            </div>
                            <div class="header-links">
                                <div class="dropdown-wrap dropdown-wrapper js-dropdown">
                                    <div class="dropdown-head js-open-dropdown"><?php echo get_field('header_btn_messangers', 'option'); ?>
                                        <i class="icon-caret"></i>
                                    </div>
                                    <div class="dropdown-list">
                                        <ul class="dropdown-phones">
                                            <li class="dropdown-phones__item">
                                                <a href="whatsapp/send?phone=<?php echo get_field('header_messanger_whatsapp', 'option'); ?>">Whatsapp</a>
                                            </li>
                                            <li class="dropdown-phones__item">
                                                <a href="viber://chat?number=<?php echo get_field('header_messanger_viber', 'option'); ?>">Viber</a>
                                            </li>
                                            <li class="dropdown-phones__item">
                                                <a href="tg://resolve?domain=<?php echo get_field('header_messanger_telegram', 'option'); ?>">Telegram</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="dropdown-wrap dropdown-wrapper js-dropdown">

                                    <div class="dropdown-head">
                                        <a href="tel:<?php echo get_field('header_phone_main', 'option'); ?>"><?php echo get_field('header_phone_main', 'option'); ?></a>
                                        <?php if ( !empty(get_field('header_phone_list', 'option')) ) { ?>
                                            <i class="icon-caret js-open-dropdown"></i>
                                        <?php } ?>
                                    </div>

                                    <?php if ( !empty(get_field('header_phone_list', 'option')) ) { ?>
                                        <div class="dropdown-list">
                                            <ul class="dropdown-phones">
                                                <?php foreach ( get_field('header_phone_list', 'option') as $phone ) { ?>
                                                <li class="dropdown-phones__item">
                                                    <a href="tel:<?= $phone['header_phone_item']; ?>"><?= $phone['header_phone_item']; ?></a>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="hamburger-box js-button-nav">
                        <div class="hamburger-inner"></div>
                        <span><?php echo get_field('header_btn_nav', 'option') ?></span>
                    </div>

                </div>

            </div>
        </div>
    </header>