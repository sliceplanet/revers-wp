<?php
/*
Template Name: Страница Услуги
*/
?>

<?php get_header(); ?>

<main class="content">
    <section class="head-inner-2 head-inner-2_contacts">
        <div class="wrapper">

            <!--    Breadcrumbs -->
            <?php if ( function_exists( 'breadcrumbs' ) ) breadcrumbs(); ?>
            <!--    End Breadcrumbs -->

            <h1 class="h2-decor wow fadeInLeftShort"><?php the_title(); ?></h1>

        </div>

        <div id="services">
            <div class="section-services">
                <div class="wrapper">

                    <div class="services-main">

                        <?php foreach ( get_field('p_main_services_list') as $i ) { ?>
                            <div class="services-main__item  wow fadeInUpShort" data-wow-delay="0.3s">
                                <div class="services-main__card">
                                    <a href="<?php echo get_category_link($i['p_main_services_item']->term_id); ?>" class="services-main__picture">
                                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" data-src="<?php echo get_field('p_archive_for_main_img', $i['p_main_services_item']) ?>" alt="" class="js-lazy" />
                                        <div class="img-hover">
                                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" data-src="<?php echo get_field('p_archive_for_main_img_hover', $i['p_main_services_item']) ?>" alt="" class="js-lazy" />
                                        </div>
                                    </a>
                                    <div class="services-main__head">
                                        <span><?= $i['p_main_services_item']->name; ?></span>
                                    </div>
                                    <?php
                                    $subcategories = get_categories(array(
                                        'child_of'   => $i['p_main_services_item']->term_id,
                                        'hide_empty' => 0
                                    ));
                                    ?>
                                    <?php if ( !empty($subcategories) ) { ?>
                                        <div class="services-main__list">
                                            <div class="services-main__head">
                                                <a href="<?php echo get_category_link($i['p_main_services_item']->term_id); ?>">
                                                    <?= $i['p_main_services_item']->name; ?>
                                                </a>
                                            </div>
                                            <ul>
                                                <?php foreach ( $subcategories as $subcat ) { ?>
                                                    <li>
                                                        <a href="<?php echo get_category_link($subcat -> term_id); ?>">
                                                            <?= $subcat -> name ?>
                                                        </a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>

    </section>
</main>


<?php get_footer(); ?>
