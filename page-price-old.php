<?php
/*
Template Name: Страница Стоимости услуг
*/
?>

<?php

$categories = get_categories ( array (
    'hide_empty' => 0,
    'parent'     => 0
) );

?>

<?php get_header(); ?>

<main class="content">

    <section class="head-inner-2">
        <div class="wrapper">
            <!--    Breadcrumbs -->
            <?php if ( function_exists( 'breadcrumbs' ) ) breadcrumbs(); ?>
            <!--    End Breadcrumbs -->

            <h1 class="h2-decor wow fadeInLeftShort"><?php echo get_field('p_price_title') ?></h1>
        </div>

        <div class="head-decor"><?php echo get_field('p_price_txt_decor') ?></div>

        <div class="head-decor-img js-bg" data-preload="<?php echo get_field('p_price_img_decor') ?>"></div>

    </section>

    <div class="js-tabs-wrap">

        <div class="price-tabs-nav-wrap">
            <div class="specialist--nav price-tabs-nav js-scroll-horizontal wow fadeInRigthShort"  data-wow-delay="0.3s">
                <div class="tabs-nav specialist-nav js-horizontal">
                    <?php $i = 1; foreach ( get_field('p_price_services_list') as $cat ) { ?>
                        <div class="tabs-nav__link js-tab-link js-slide" data-href="#tab-<?= $i++; ?>">
                            <?= $cat['p_price_services_item'] -> name?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

        <div class="price-tabs-wrap">
            <div class="wrapper">
                <div class="tabs-wrap price-tabs wow fadeInUpShort" data-wow-delay="0.6s">
                    <?php $j = 1; foreach ( get_field('p_price_services_list') as $cat ) { ?>
                        <div class="tab js-tab" id="tab-<?= $j++; ?>">
                            <div class="faq-list price-list">
                                <?php
                                $subcategories = get_categories(array(
                                    'child_of'   => $cat['p_price_services_item'] -> term_id,
                                    'hide_empty' => 0
                                ));
                                ?>
                                <?php if ( !empty($subcategories) ) { ?>
                                    <?php foreach ( $subcategories as $subcat ) { ?>
                                        <div class="faq-list__item js-accordion-wrap wow fadeInUpShort"  data-wow-delay="0.2s">

                                            <div class="faq-list__title js-accordion-head">
                                                <span><?= $subcat -> name ?></span>
                                                <span class="faq-list__icon">
                                            <i class="icon-plus"></i>
                                        </span>
                                            </div>

                                            <div class="faq-list__info js-accordion-body">
                                                <div class="services-price">
                                                    <?php
                                                    global $post;
                                                    $categorie = $subcat -> name;
                                                    $mypost_Query = new WP_Query ( array(
                                                        'category_name'    => $categorie,
                                                        'post_type'        => 'post',
                                                        'post_status'      => 'publish',
                                                        'posts_per_page'   => -1,
                                                    ) );
                                                    if ( $mypost_Query->have_posts() ) {
                                                        while ( $mypost_Query->have_posts() ) { $mypost_Query->the_post(); ?>
                                                            <?php
                                                            $price     = get_field ( 'post_service_price', $post );
                                                            $old_price = get_field ( 'post_service_old_price', $post );
                                                            $stock     = get_field ( 'post_service_stock', $post );
                                                            ?>

                                                            <div class="services-price__item">
                                                                <div class="services-price__head">
                                                                    <a href="<?php echo get_permalink(); ?>"
                                                                       class="services-price__title"><?= $post->post_title ?></a>
                                                                    <?php if ($stock == true) { ?>
                                                                        <a class="btn-2 services-price__action">Акция</a>
                                                                    <?php } ?>
                                                                </div>
                                                                <div class="services-price__right">
                                                                    <div class="services-price__counts">
                                                                        <?php if ( !empty( $old_price ) ) { ?>
                                                                            <span class="services-price__counts-currency services-price__new"><?= $price ?></span>
                                                                            <span class="services-price__old-price"><?= $old_price ?></span>
                                                                        <?php } else { ?>
                                                                            <span class="services-price__counts-currency"><?= $price ?></span>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="services-price__btn">
                                                                        <span data-src="#order" class="btn btn_service-order js-fancybox">заказать</span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        <?php }
                                                    } else { echo('<p>Извините, нет услуг.</p>'); }
                                                    wp_reset_postdata(); ?>

                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } else { ?>
                                    <div class="faq-list__item js-accordion-wrap wow fadeInUpShort"  data-wow-delay="0.2s">
                                        <div class="faq-list__info js-accordion-body" style="display: block;">
                                            <div class="services-price">
                                                <?php
                                                global $post;
                                                $categorie = $cat['p_price_services_item'] -> name;
                                                $mypost_Query = new WP_Query ( array(
                                                    'category_name'    => $categorie,
                                                    'post_type'        => 'post',
                                                    'post_status'      => 'publish',
                                                    'posts_per_page'   => -1,
                                                ) );
                                                if ( $mypost_Query->have_posts() ) {
                                                    while ( $mypost_Query->have_posts() ) { $mypost_Query->the_post(); ?>
                                                        <?php
                                                        $price     = get_field ( 'post_service_price', $post );
                                                        $old_price = get_field ( 'post_service_old_price', $post );
                                                        $stock     = get_field ( 'post_service_stock', $post );
                                                        ?>

                                                        <div class="services-price__item">
                                                            <div class="services-price__head">
                                                                <a href="<?php echo get_permalink(); ?>"
                                                                   class="services-price__title"><?= $post->post_title ?></a>
                                                                <?php if ($stock == true) { ?>
                                                                    <a class="btn-2 services-price__action">Акция</a>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="services-price__right">
                                                                <div class="services-price__counts">
                                                                    <?php if ( !empty( $old_price ) ) { ?>
                                                                        <span class="services-price__counts-currency services-price__new"><?= $price ?></span>
                                                                        <span class="services-price__old-price"><?= $old_price ?></span>
                                                                    <?php } else { ?>
                                                                        <span class="services-price__counts-currency"><?= $price ?></span>
                                                                    <?php } ?>
                                                                </div>
                                                                <div class="services-price__btn">
                                                                    <span data-src="#order" class="btn btn_service-order js-fancybox">заказать</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    <?php }
                                                } else { echo('<p>Извините, нет услуг.</p>'); }
                                                wp_reset_postdata(); ?>

                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

    </div>

</main>

<?php get_footer(); ?>

<script>
    $('.services-price__item').each(function () {
        var title  = $(this).find('.services-price__title').text();
        var btn    = $(this).find('.btn_service-order');

        btn.on('click', function () {
            var select = $('#order .jq-selectbox__select-text');
            select.text(title);

            $('.jq-selectbox__dropdown ul li').each(function () {
                if ( $(this).text() == select.text() ) {
                    $(this).addClass('selected sel');
                } else {
                    $(this).removeClass('selected sel');
                }
            });

            $('select.wpcf7-select option').each(function () {
                if ( $(this).text() == select.text() ) {
                    $(this).attr('selected', '');
                }
            });
        });
    });
</script>
