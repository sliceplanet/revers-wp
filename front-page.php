<?php
/*
Template Name: Главная страница
*/
?>

<?php get_header(); ?>


<main class="content">

    <div class="section-main" id="main">
        <div class="main-bl">
            <div class="slider-main sliders-arrow js-slider-main">
                <?php foreach ( get_field('p_main_top_slider_list') as $item ) { ?>
                    <div class="slider-main__item">
                        <div class="wrapper">
                            <div class="slider-main__card">
                                <div class="main-bl__head">
                                    <div class="main-bl__heading">
                                        <div class="main-title"><?= $item['p_main_top_slider_item_title']; ?></div>
                                        <?php if ( !empty($item['p_main_top_slider_item_subtitle']) ) { ?>
                                            <div class="main-subtitle"><?= $item['p_main_top_slider_item_subtitle'] ?></div>
                                        <?php } ?>
                                    </div>
                                    <div class="main-bl__arrow"> <a href="<?= $item['p_main_top_slider_item_link'] ?>" class="arrow-1">смотреть</a>
                                    </div>
                                </div>
                                <div class="main-bl__picture">
                                    <img src="<?= $item['p_main_top_slider_item_img'] ?>"
                                         alt="" />
                                </div>
                            </div>
                            <div class="slider-main__decor" style=" background: <?= $item['p_main_top_slider_item_color'] ?>; "></div>
                            <div class="slider-main-decor"></div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="wrapper main-text">
            <div class="h1-main"><?php echo get_field('p_main_top_decor') ?></div>
        </div>

        <div class="main-arrows js-append-main-arrow"></div>
        <div class="wrapper main-decor"></div>
    </div>

    <div id="services">
        <div class="section-services">
            <div class="wrapper">

                <div class="h2-decor wow fadeInLeftShort"><?php echo get_field('p_main_services_title'); ?></div>

                <div class="services-main">

                    <?php foreach ( get_field('p_main_services_list') as $i ) { ?>
                        <div class="services-main__item  wow fadeInUpShort" data-wow-delay="0.3s">
                            <div class="services-main__card">
                                <a href="<?php echo get_category_link($i['p_main_services_item']->term_id); ?>" class="services-main__picture">
                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" data-src="<?php echo get_field('p_archive_for_main_img', $i['p_main_services_item']) ?>" alt="" class="js-lazy" />
                                    <div class="img-hover">
                                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" data-src="<?php echo get_field('p_archive_for_main_img_hover', $i['p_main_services_item']) ?>" alt="" class="js-lazy" />
                                    </div>
                                </a>
                                <div class="services-main__head">
                                    <span><?= $i['p_main_services_item']->name; ?></span>
                                </div>
                                <?php
//                                $subcategories = get_categories(array(
//                                    'child_of'   => $i['p_main_services_item']->term_id,
//                                    'hide_empty' => 0
//                                ));
                                $subcategories = get_posts ( array(
                                    'post_type'   => 'post',
                                    'numberposts' => -1,
                                    'category'    => $i['p_main_services_item']->term_id,
                                ) );
                                ?>
                                <?php if ( !empty($subcategories) ) { ?>
                                    <div class="services-main__list">
                                        <div class="services-main__head">
                                            <a href="<?php echo get_category_link($i['p_main_services_item']->term_id); ?>">
                                                <?= $i['p_main_services_item']->name; ?>
                                            </a>
                                        </div>
                                        <ul>
                                            <?php foreach ( $subcategories as $subcat ) { ?>
                                                <?php if ( get_field('post_get_on_front_page', $subcat ) == true ) { ?>
                                                    <li>
                                                        <a href="<?= $subcat -> guid ?>">
                                                            <?= $subcat -> post_title ?>
                                                        </a>
                                                    </li>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                <?php } ?>

                            </div>
                        </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>

    <div class="section-pick-up-procedure">
        <div class="wrapper">
            <div class="h2-decor mob-show"><?php echo get_field('p_main_choose_title') ?></div>
            <div class="pick-up-procedure">
                <div class="pick-up-procedure__picture with-decor">
                    <div class="with-decor__inner">
                        <div class="section-picture  wow fadeInLeftShort" data-wow-delay="0.3s">
                            <div class="section-picture__img js-lazy" data-src="<?php echo get_field('p_main_choose_img') ?>"></div>
                        </div>
                    </div>
                    <div class="decor-left"></div>
                </div>
                <div class="pick-up-procedure__description wow fadeInUpShort">
                    <div class="h2-decor mob-hide"><?php echo get_field('p_main_choose_title') ?></div>
                    <div class="section-text pick-up-procedure__text scroll-unvisible js-scroll">
                        <p><?php echo get_field('p_main_choose_txt') ?></p>
                    </div>
                    <div class="pick-up-procedure__button">
                        <a href="<?php echo get_field('p_main_choose_btn_link') ?>" class="btn btn_pick"><?php echo get_field('p_main_choose_btn_name') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-worth">
        <div class="wrapper">
            <div class="worth-wrapper worth-wrapper_main js-slider-group">
                <div class="worth-description">
                    <div class="slider-nav slider-nav_worth scroll-unvisible js-slider-1_nav js-scroll-horizontal wow fadeInLeftShort"  data-wow-delay="0.5s">
                        <div class="slider-nav__slides js-horizontal">
                            <?php foreach ( get_field('p_main_worth_list') as $item ) { ?>
                                <div class="slider-nav__item js-slider-1_nav-item js-slide">
                                    <div class="slider-nav__card js-lazy"
                                         data-src="<?= $item['p_main_worth_item_img']; ?>">
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="js-slider-1-1 wow fadeInUpShort">
                        <?php foreach ( get_field('p_main_worth_list') as $item ) { ?>
                            <div class="worth-description__item">
                            <div class="worth__description">
                                <div class="h2-decor mob-hide"><?= $item['p_main_worth_item_title'] ?></div>
                                <div class="section-text worth__text scroll-unvisible js-scroll">
                                    <p><?= $item['p_main_worth_item_txt'] ?></p>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="worth-img wow fadeInRigthShort"  data-wow-delay="0.3s">
                    <div class="worth js-slider-1">
                        <?php foreach ( get_field('p_main_worth_list') as $item ) { ?>
                            <div class="worth__item">
                                <div class="worth__card">
                                    <div class="worth__picture with-decor">
                                        <div class="with-decor__inner">
                                            <div class="h2-decor mob-show"><?= $item['p_main_worth_item_title'] ?></div>
                                            <div class="section-picture">
                                                <div class="section-picture__img js-lazy"
                                                     data-src="<?= $item['p_main_worth_item_img'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="decor-right decor-worth js-lazy"
                 data-src="<?php echo get_field('p_main_worth_decor') ?>"></div>

        </div>
    </div>

    <?php get_template_part('template-parts/specialists')?>

    <div class="section-equipment">
        <div class="wrapper">

            <div class="h2-decor wow fadeInLeftShort"><?php echo get_field('p_main_equip_title'); ?></div>

            <div class="equipment js-tabs-wrap">
                <div class="equipment-nav-wrap scroll-unvisible js-scroll js-scroll-horizontal  wow fadeInLeftShort" data-wow-delay="0.6s">
                    <div class="tabs-nav equipment-nav js-horizontal">
                        <?php $j = 1; foreach ( get_field('p_main_equip_list') as $item ) { ?>
                            <div class="tabs-nav__link js-tab-link js-slide" data-href="#equipment<?= $j++; ?>"><?= $item['p_main_equip_item_name'] ?></div>
                        <?php } ?>
                    </div>
                </div>

                <div class="tabs-wrap equipment-tab wow fadeInUpShort" data-wow-delay="0.3s">
                    <div class="equipment-tab-wrap">
                        <?php $i = 1; foreach ( get_field('p_main_equip_list') as $item ) { ?>
                            <div class="tab js-tab" id="equipment<?= $i++; ?>">
                                <div class="equipment-sliders js-slider-group">

                                    <div class="equipment-sliders__left">
                                        <div class="equipment-slider js-slider-equipment-1">
                                            <?php foreach ( $item['p_main_equip_item_slider'] as $slide ) { ?>
                                                <div class="equipment-slider__item">
                                                <div class="section-picture equipment-slider__picture">
                                                    <div class="section-picture__img js-lazy"
                                                         data-src="<?= $slide['p_main_equip_item_slide'] ?>"></div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>

                                        <div class="slider-nav scroll-unvisible slider-nav_equipment js-slider-equipment_nav js-scroll-horizontal">
                                            <div class="slider-nav__slides js-horizontal">
                                                <?php foreach ( $item['p_main_equip_item_slider'] as $slide ) { ?>
                                                    <div class="slider-nav__item js-slider-equipment_nav-item js-slide">
                                                    <div class="slider-nav__card js-lazy"
                                                         data-src="<?= $slide['p_main_equip_item_slide'] ?>">
                                                    </div>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="equipment-sliders__right">
                                        <div class="equipment-slider-description">
                                            <div class="equipment-slider-description__item">
                                                <div class="equipment-slider-description__title"><?= $item['p_main_equip_item_title'] ?></div>
                                                <div class="equipment-slider-description__text scroll-unvisible js-scroll">
                                                    <p><?= $item['p_main_equip_item_txt'] ?></p>
                                                </div>
                                                <div class="equipment-slider-description__servises">
                                                    <div class="equipment-slider-description__subtitle"><?= $item['p_main_equip_item_subtitle'] ?></div>
                                                </div>
                                                <div class="list-decor list-decor-2">
                                                    <?= $item['p_main_equip_item_list_of_procedures'] ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="decor-right decor-equipment"></div>

                </div>
            </div>
        </div>
    </div>

    <div class="section-compare">
        <div class="wrapper">

            <div class="h2-decor wow fadeInUpShort" data-wow-delay="0.3s">фото до и после</div>

            <div class="compare-wrap">

                <div class="worth-wrapper js-slider-group">
                    <div class="worth-img compare__picture wow fadeInLeftShort">
                        <div class="worth js-slider-1">
                            <?php foreach ( get_field('p_main_compare_list') as $item ) { ?>
                                <div class="worth__item">
                                    <div class="compare__picture with-decor">
                                        <div class="with-decor__inner">
                                            <div class="compare-img js-compare-img">
                                                <!-- The before image is first -->
                                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
                                                     data-src="<?= $item['p_main_compare_item_before_img'] ?>"
                                                     alt=""
                                                     class="comparison-image js-img" />
                                                <!-- The after image is last -->
                                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
                                                     data-src="<?= $item['p_main_compare_item_after_img'] ?>"
                                                     alt=""
                                                     class="comparison-image js-img" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="worth-description compare-description">
                        <div class="slider-nav slider-compare-nav scroll-unvisible js-slider-1_nav js-scroll-horizontal wow fadeInUpShort" data-wow-delay="0.6s">
                            <div class="slider-nav__slides js-horizontal">
                                <?php foreach ( get_field('p_main_compare_list') as $item ) { ?>
                                    <div class="slider-nav__item js-slider-1_nav-item js-slide">
                                        <div class="slider-nav__card js-lazy"
                                             data-src="<?= $item['p_main_compare_item_nav_img'] ?>"></div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="js-slider-1-1 wow fadeInUpShort" data-wow-delay="0.3s">
                            <?php foreach ( get_field('p_main_compare_list') as $item ) { ?>
                                <div class="worth-description__item">
                                <div class="compare__description">
                                    <div class="compare__title"><?= $item['p_main_compare_item_title'] ?></div>
                                    <div class="compare__text scroll-unvisible js-scroll">
                                        <p><?= $item['p_main_compare_item_desc'] ?></p>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <div class="decor-left decor-compare wow fadeInLeftShort"></div>
            </div>
        </div>
    </div>

    <div class="section-reviews">
        <div class="wrapper">

            <div class="h2-decor wow fadeInLeftShort"><?php echo get_field('p_main_reviews_title'); ?></div>

            <div class="reviews js-tabs-wrap">
                <div class="tabs-nav reviews-nav js-scroll-horizontal wow fadeInRigthShort" data-wow-delay="0.3s">
                    <div class="slider-nav__slides reviews-slides js-horizontal">
                        <?php foreach ( get_field('p_main_reviews_tabs') as $item ) { ?>
                            <div class="tabs-nav__link js-tab-link js-slide" data-href="#<?= $item['p_main_reviews_tab_id'] ?>">
                                <?= $item['p_main_reviews_tab_name'] ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="tabs-wrap review-tab wow fadeInUpShort" data-wow-delay="0.6s">
                    <?php foreach ( get_field('p_main_reviews_tabs') as $item ) { ?>
                        <div class="tab js-tab" id="<?= $item['p_main_reviews_tab_id'] ?>">
                            <div class="slider-reviews sliders-arrow js-slider-reviews">
                                <?php foreach ( $item['p_main_reviews_tab_list'] as $slide ) { ?>
                                    <div class="slider-reviews__item">
                                        <div class="slider-reviews__head"> <i class="icon-quote slider-reviews__icon"></i>
                                            <span class="slider-reviews__author"><?= $slide['p_main_reviews_tab_item_name'] ?></span>
                                            <span class="slider-reviews__date"><?= $slide['p_main_reviews_tab_item_date'] ?></span>
                                        </div>
                                        <div class="slider-reviews__description">
                                            <p><?= $slide['p_main_reviews_tab_item_desc'] ?></p>
                                        </div>
                                        <div class="slider-reviews__details">
                                            <div class="slider-reviews__details-col">
                                                <div class="slider-reviews__details-val"><?= $slide['p_main_reviews_tab_item_doc_name'] ?></div>
                                                <div class="slider-reviews__details-label">Специалист:</div>
                                            </div>
                                            <div class="slider-reviews__details-col">
                                                <div class="slider-reviews__details-val"><?= $slide['p_main_reviews_tab_item_service'] ?></div>
                                                <div class="slider-reviews__details-label">Услуга:</div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

</main>


<?php get_footer(); ?>
