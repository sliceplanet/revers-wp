<?php

add_action( 'wp_enqueue_scripts', 'theme_scripts' );

function theme_scripts() {

    //  Adding styles


    //  Adding scripts
//    wp_deregister_script( 'jquery' );

//    wp_enqueue_script( 'jquery',            get_template_directory_uri() . '/js/components/jquery.min.js', array(), '1.0.0', true );
//    wp_enqueue_script( 'jquery-migrate',    get_template_directory_uri() . '/js/components/jquery-migrate.min.js', array(), '1.0.0', true );

//    wp_enqueue_script( 'fancybox',          get_template_directory_uri() . '/js/components/jquery.fancybox.min.js', array(), '1.0.0', true );

//    wp_enqueue_script( 'jquery-event',      get_template_directory_uri() . '/js/components/jquery.event.move.js', array(), '1.0.0', true );
//    wp_enqueue_script( 'jquery-formstyler', get_template_directory_uri() . '/js/components/jquery.formstyler.js', array(), '1.0.0', true );
//    wp_enqueue_script( 'jquery-validate',   get_template_directory_uri() . '/js/components/jquery.validate.min.js', array(), '1.0.0', true );
//    wp_enqueue_script( 'jquery-ui',         get_template_directory_uri() . '/js/components/jquery-ui.min.js', array(), '1.0.0', true );
//    wp_enqueue_script( 'draggable',         get_template_directory_uri() . '/js/components/Draggable.min.js', array(), '1.0.0', true );

    wp_enqueue_script( 'libs',   get_template_directory_uri() . '/js/libs.js', array(), '1.0.0', true );

    wp_enqueue_script( 'custom', get_template_directory_uri() . '/js/custom.js', array(), '1.0.0', true );
    wp_enqueue_script( 'map',    get_template_directory_uri() . '/js/maps.js', array(), '1.0.0', true );

}

// Enable svg
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');

add_theme_support('menus');
add_theme_support('post-thumbnails');

/*Header and Footer fields*/
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Theme General Settings',
        'menu_title'	=> 'Theme Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));

    acf_add_options_sub_page( array (
        'page_title' 	=> 'Theme Header Settings',
        'menu_title'	=> 'Header',
        'parent_slug'	=> 'theme-general-settings',
    ));

    acf_add_options_sub_page( array (
        'page_title' 	=> 'Theme Footer Settings',
        'menu_title'	=> 'Footer',
        'parent_slug'	=> 'theme-general-settings',
    ));

    acf_add_options_sub_page( array (
        'page_title' 	=> 'Navigation Settings',
        'menu_title'	=> 'Navigation',
        'parent_slug'	=> 'theme-general-settings',
    ));

    acf_add_options_sub_page( array (
        'page_title' 	=> 'Services Settings',
        'menu_title'	=> 'Services',
        'parent_slug'	=> 'theme-general-settings',
    ));

    acf_add_options_sub_page( array (
        'page_title' 	=> 'Specialists Settings',
        'menu_title'	=> 'Specialists',
        'parent_slug'	=> 'theme-general-settings',
    ));

}


/***Custom post type Articles***/
add_action('init', 'register_article_post_type');
function register_article_post_type(){
    register_post_type('article', array(
        'labels'               => array(
            'name'               => 'Статьи', // Основное название типа записи
            'singular_name'      => 'Статьи', // отдельное название записи типа Book
            'add_new'            => 'Добавить статью',
            'add_new_item'       => 'Добавить новую статью',
            'edit_item'          => 'Редактировать статью',
            'new_item'           => 'Новая статья',
            'view_item'          => 'Посмотреть статью',
            'search_items'       => 'Найти статью',
            'not_found'          => 'Статья не найдена',
            'not_found_in_trash' => 'В корзине статья не найден',
            'parent_item_colon'  => '',
            'menu_name'          => 'Статьи'

        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 5,
        'rewrite'            => array( 'slug' => 'article', 'with_front' => false, 'hierarchical ' => true  ),
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
    ) );
}

add_action('init', 'articlecat_taxonomy');
function articlecat_taxonomy(){
    register_taxonomy('articlelist', array('article'), array(
        'label'               => '', // определяется параметром $labels->name
        'labels'              => array(
            'name'              => 'Рубрикa',
            'singular_name'     => 'Рубрикa',
            'search_items'      => 'Искать рубрику',
            'all_items'         => 'Все рубрики',
            'view_item '        => 'Показать рубрику',
            'parent_item'       => 'Родит. рубрика',
            'parent_item_colon' => 'Родит. рубрика:',
            'edit_item'         => 'Редактировать',
            'update_item'       => 'Обновить рубрику',
            'add_new_item'      => 'Добавить рубрику',
            'new_item_name'     => 'Новая рубрика',
            'menu_name'         => 'Рубрика',
        ),
        'description'           => 'Рубрики статей', // описание таксономии
        'public'                => true,
        'show_in_nav_menus'     => false, // равен аргументу public
        'show_ui'               => true, // равен аргументу public
        'show_tagcloud'         => false, // равен аргументу show_ui
        'hierarchical'          => true,
        'rewrite'               => array('slug'=>'articlelist', 'hierarchical'=>false, 'with_front'=>false ),
        'show_admin_column'     => true, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
    ) );
}


/*Breadcrumbs*/
function breadcrumbs() {

    $text['home']     = 'Главная';
    $text['category'] = '%s';
    $text['search']   = 'Результаты поиска по запросу "%s"';
    $text['tag']      = 'Записи с тегом "%s"';
    $text['author']   = 'Статьи автора %s';
    $text['404']      = 'Ошибка 404';
    $text['page']     = 'Страница %s';
    $text['cpage']    = 'Страница комментариев %s';

    $wrap_before    = '<div class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList"><div class="wrapper bread-crumbs__wrap"><ul class="breadcrumb">'; // открывающий тег обертки
    $wrap_after     = '</ul></div></div><!-- .breadcrumbs -->';
    $before         = '<li class="breadcrumbs__current">';
    $after          = '</li>';

    $show_on_home   = 0;
    $show_home_link = 1;
    $show_current   = 1;
    $show_last_sep  = 1;

    global $post;
    $home_url       = home_url('/');
    $link           = '<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
    $link          .= '<a class="breadcrumbs__link" href="%1$s" itemprop="item"><span itemprop="name">%2$s</span></a>';
    $link          .= '<meta itemprop="position" content="%3$s" />';
    $link          .= '</li>';
    $parent_id      = ( $post ) ? $post->post_parent : '';
    $home_link      = sprintf( $link, $home_url, $text['home'], 1 );

    if ( is_home() || is_front_page() ) {

        if ( $show_on_home ) echo $wrap_before . $home_link . $wrap_after;

    } else {

        $position = 0;

        echo $wrap_before;

        if ( $show_home_link ) {
            $position += 1;
            echo $home_link;
        }

        if ( is_category() ) {
            $parents = get_ancestors( get_query_var('cat'), 'category' );
            foreach ( array_reverse( $parents ) as $cat ) {
                $position += 1;
                if ( $position > 1 );
                echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
            }
            if ( get_query_var( 'paged' ) ) {
                $position += 1;
                $cat = get_query_var('cat');
                echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
                echo $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
            } else {
                if ( $show_current ) {
                    if ( $position >= 1 );
                    echo $product . $before . sprintf( $text['category'], single_cat_title( '', false ) ) . $after;
                } elseif ( $show_last_sep );
            }

        } elseif ( is_search() ) {
            if ( get_query_var( 'paged' ) ) {
                $position += 1;
                if ( $show_home_link );
                echo sprintf( $link, $home_url . '?s=' . get_search_query(), sprintf( $text['search'], get_search_query() ), $position );
                echo $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
            } else {
                if ( $show_current ) {
                    if ( $position >= 1 );
                    echo $before . sprintf( $text['search'], get_search_query() ) . $after;
                } elseif ( $show_last_sep );
            }

        } elseif ( is_year() ) {
            if ( $show_home_link && $show_current );
            if ( $show_current ) echo $before . get_the_time('Y') . $after;
            elseif ( $show_home_link && $show_last_sep );

        } elseif ( is_month() ) {
            if ( $show_home_link );
            $position += 1;
            echo sprintf( $link, get_year_link( get_the_time('Y') ), get_the_time('Y'), $position );
            if ( $show_current ) echo $before . get_the_time('F') . $after;
            elseif ( $show_last_sep );

        } elseif ( is_day() ) {
            if ( $show_home_link );
            $position += 1;
            echo sprintf( $link, get_year_link( get_the_time('Y') ), get_the_time('Y'), $position )/* . $sep */;
            $position += 1;
            echo sprintf( $link, get_month_link( get_the_time('Y'), get_the_time('m') ), get_the_time('F'), $position );
            if ( $show_current ) $before . get_the_time('d') . $after;
            elseif ( $show_last_sep );

        } elseif ( is_single() && ! is_attachment() ) {
            if ( get_post_type() != 'post' ) {
                $position += 1;
                $post_type = get_post_type_object( get_post_type() );
                if ( $position > 1 );
                echo sprintf( $link, get_post_type_archive_link( $post_type->name ), $post_type->labels->name, $position );
                if ( $show_current ) echo  $before . get_the_title() . $after;
                elseif ( $show_last_sep );
            } else {
                $cat = get_the_category(); $catID = $cat[0]->cat_ID;
                $parents = get_ancestors( $catID, 'category' );
                $parents = array_reverse( $parents );
                $parents[] = $catID;
                foreach ( $parents as $cat ) {
                    $position += 1;
                    if ( $position > 1 );
                    echo $product . sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
                }
                if ( get_query_var( 'cpage' ) ) {
                    $position += 1;
                    echo sprintf( $link, get_permalink(), get_the_title(), $position );
                    echo $before . sprintf( $text['cpage'], get_query_var( 'cpage' ) ) . $after;
                } else {
                    if ( $show_current ) echo $before . get_the_title() . $after;
                    elseif ( $show_last_sep ) ;
                }
            }

        } elseif ( is_post_type_archive() ) {
            $post_type = get_post_type_object( get_post_type() );
            if ( get_query_var( 'paged' ) ) {
                $position += 1;
                if ( $position > 1 );
                echo sprintf( $link, get_post_type_archive_link( $post_type->name ), $post_type->label, $position );
                echo $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
            } else {
                if ( $show_home_link && $show_current );
                if ( $show_current ) echo $before . $post_type->label . $after;
                elseif ( $show_home_link && $show_last_sep );
            }

        } elseif ( is_attachment() ) {
            $parent = get_post( $parent_id );
            $cat = get_the_category( $parent->ID ); $catID = $cat[0]->cat_ID;
            $parents = get_ancestors( $catID, 'category' );
            $parents = array_reverse( $parents );
            $parents[] = $catID;
            foreach ( $parents as $cat ) {
                $position += 1;
                if ( $position > 1 );
                echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
            }
            $position += 1;
            echo sprintf( $link, get_permalink( $parent ), $parent->post_title, $position );
            if ( $show_current ) echo $before . get_the_title() . $after;
            elseif ( $show_last_sep );

        } elseif ( is_page() && ! $parent_id ) {
            if ( $show_home_link && $show_current );
            if ( $show_current ) echo $before . get_the_title() . $after;
            elseif ( $show_home_link && $show_last_sep );

        } elseif ( is_page() && $parent_id ) {
            $parents = get_post_ancestors( get_the_ID() );
            foreach ( array_reverse( $parents ) as $pageID ) {
                $position += 1;
                if ( $position > 1 );
                echo sprintf( $link, get_page_link( $pageID ), get_the_title( $pageID ), $position );
            }
            if ( $show_current ) echo $before . get_the_title() . $after;
            elseif ( $show_last_sep );

        } elseif ( is_tag() ) {
            if ( get_query_var( 'paged' ) ) {
                $position += 1;
                $tagID = get_query_var( 'tag_id' );
                echo sprintf( $link, get_tag_link( $tagID ), single_tag_title( '', false ), $position );
                echo $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
            } else {
                if ( $show_home_link && $show_current );
                if ( $show_current ) echo $before . sprintf( $text['tag'], single_tag_title( '', false ) ) . $after;
                elseif ( $show_home_link && $show_last_sep );
            }

        } elseif ( is_author() ) {
            $author = get_userdata( get_query_var( 'author' ) );
            if ( get_query_var( 'paged' ) ) {
                $position += 1;
                echo sprintf( $link, get_author_posts_url( $author->ID ), sprintf( $text['author'], $author->display_name ), $position );
                echo $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
            } else {
                if ( $show_home_link && $show_current );
                if ( $show_current ) echo $before . sprintf( $text['author'], $author->display_name ) . $after;
                elseif ( $show_home_link && $show_last_sep );
            }

        } elseif ( is_404() ) {
            if ( $show_home_link && $show_current );
            if ( $show_current ) echo $before . $text['404'] . $after;
            elseif ( $show_last_sep );

        } elseif ( has_post_format() && ! is_singular() ) {
            if ( $show_home_link && $show_current );
            echo get_post_format_string( get_post_format() );
        }

        echo $wrap_after;

    }
}


/* Services in select input */
function pine_dynamic_select_field_values ( $scanned_tag, $replace ) {

    if ( $scanned_tag['name'] != 'servicesOrder' )
        return $scanned_tag;

    $rows = get_posts(
        array (
            'post_type' => 'post',
            'numberposts' => -1,
            'orderby' => 'title',
            'order' => 'ASC'
        )
    );

    if ( ! $rows )
        return $scanned_tag;

    foreach ( $rows as $row ) {
        $scanned_tag['raw_values'][] = $row->post_title . '|' . $row->post_title;
    }

    $pipes = new WPCF7_Pipes($scanned_tag['raw_values']);

    $scanned_tag['values'] = $pipes->collect_befores();
    $scanned_tag['pipes'] = $pipes;

    return $scanned_tag;
}

add_filter( 'wpcf7_form_tag', 'pine_dynamic_select_field_values', 10, 2);
/* End Services in select input */



/* Dissable plugins update */
add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );
function filter_plugin_updates( $value ) {
    unset( $value->response['advanced-custom-fields-pro/acf.php'] );
    return $value;
}


function edit_admin_menus() {
    global $menu;
    global $submenu;

    $menu[5][0] = 'Услуги'; // Изменить Записи на Лошади
    $submenu['edit.php'][5][0] = 'Все услуги';
    $submenu['edit.php'][10][0] = 'Добавить услугу';
    $submenu['edit.php'][15][0] = 'Категории услуг';
}
add_action( 'admin_menu', 'edit_admin_menus' );