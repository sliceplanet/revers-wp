<?php
/*
 * Template name: Услуга
 * Template post type: post
 */
?>

<?php get_header(); ?>

<main class="content">

    <section class="head-inner header-services head-inner_offset">

        <div class="wrapper wrapper-inner-head">

            <!--    Breadcrumbs -->
            <?php if ( function_exists( 'breadcrumbs' ) ) breadcrumbs(); ?>
            <!--    End Breadcrumbs -->

            <div class="head-inner__heading">
                <h1 class="h2-decor h1-inner"><?php the_title(); ?></h1>
                <div class="section-text">
                    <p><?php echo get_field('post_service_tit_txt'); ?></p>
                </div>
            </div>
        </div>

        <div class="bg-head js-bg" data-preload="<?php echo get_field('post_service_tit_bgi'); ?>"></div>

    </section>

    <div class="section-services-description section-service-description">
        <div class="wrapper">
            <div class="worth-wrapper worth-wrapper_main">
                <div class="worth-description wow fadeInUpShort">
                    <div class="worth__description">
                        <h2 class="h2-decor mob-hide"><?php echo get_field('post_service_desc_title'); ?></h2>
                        <div class="section-text worth__text worth__text-2 scroll-unvisible js-scroll">
                            <p><?php echo get_field('post_service_desc_txt'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="worth-img wow fadeInRigthShort" data-wow-delay="0.3s">
                    <div class="worth">
                        <div class="worth__item">
                            <div class="worth__card">
                                <div class="worth__picture with-decor">
                                    <div class="with-decor__inner">
                                        <h2 class="h2-decor mob-show"><?php echo get_field('post_service_desc_title'); ?></h2>
                                        <div class="section-picture">
                                            <div class="section-picture__img js-lazy" data-src="<?php echo get_field('post_service_desc_img'); ?>"></div>
                                        </div>
                                    </div>
                                    <div class="decor-right decor-worth decor-services decor-description"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <section class="section-testimony">
        <div class="wrapper">
            <h2 class="h2-decor wow fadeInLeftShort"><?php echo get_field('post_service_testimony_title') ?></h2>
            <div class="testimony">
                <div class="list-decor list-testimony wow fadeInUpShort" data-wow-dalay="0.3s">
                    <?php echo get_field('post_service_testimony_list') ?>
                </div>
                <div class="specialist-note wow fadeInUpShort">
                    <div class="specialist-note__text"><?php echo get_field('post_service_testimony_txt') ?></div>
                    <div class="specialist-note__img">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" data-src="<?php echo get_field('post_service_testimony_img') ?>" alt="" class="js-lazy">
                    </div>
                    <img  class="specialist-note__decor js-lazy"   src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" data-src="<?php echo get_field('post_service_testimony_decor') ?>" alt="">
                </div>
            </div>
        </div>
    </section>
    
    <div class="section-10">
        <div class="wrapper wow fadeInUpShor">
            <?php if ( !empty(get_field('post_service_testimony_seo_txt')) ) { ?>
            <div class="section-text section-text-10">
                <p><?php echo get_field('post_service_testimony_seo_txt') ?></p>
            </div>
            <?php } ?>

            <div class="block-10 js-slider-group">
                <div class="equipment-sliders__left block-10__left wow fadeInLeftShort"  data-wow-dalay="0.3s">
                    <div class="equipment-slider js-slide-1">
                        <?php foreach ( get_field('post_service_equipment_list') as $item ) { ?>
                            <div class="equipment-slider__item">
                                <div class="section-picture equipment-slider__picture">
                                    <div class="section-picture__img js-lazy" data-src="<?= $item['post_service_equipment_item_main_img'] ?>"></div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="slider-nav scroll-unvisible slider-nav_equipment js-slide-1_navs js-scroll-horizontal">
                        <div class="slider-nav__slides js-horizontal">
                            <?php foreach ( get_field('post_service_equipment_list') as $item ) { ?>
                                <div class="slider-nav__item js-slide-1_nav js-slide">
                                    <div class="slider-nav__card js-bg" data-preload="<?= $item['post_service_equipment_item_nav_img'] ?>"></div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                </div>

                <div class="equipment-sliders__right block-10__right wow fadeInRigthShort" data-wow-dalay="0.6s">
                    <div class="block-10-description js-slide-1_text">
                        <?php foreach ( get_field('post_service_equipment_list') as $item ) { ?>
                            <div class="equipment-slider-description__item">
                                <div class="equipment-slider-description__title"><?= $item['post_service_equipment_item_title'] ?></div>
                                <div class="equipment-slider-description__text scroll-unvisible js-scroll">
                                    <p><?= $item['post_service_equipment_item_txt'] ?></p>
                                </div>
                                <div class="list-decor list-decor-3">
                                    <div class="list-decor-3__col">
                                        <div class="list-decor-3__title"><?= $item['post_service_equipment_item_equip_title'] ?></div>
                                        <?= $item['post_service_equipment_item_equip_txt'] ?>
                                    </div>
                                    <div class="list-decor-3__col">
                                        <div class="list-decor-3__title"><?= $item['post_service_equipment_item_antiequip_title'] ?></div>
                                        <?= $item['post_service_equipment_item_antiequip_txt'] ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="decor-left decor-10 js-lazy" data-src="<?php bloginfo('template_url') ?>/img/examples/block-10-bg.jpg">
                </div>
            </div>
        </div>
    </div>
    
    <section class="section-stages">
        <div class="wrapper">
            <h2 class="h2-decor wow fadeInLeftShort"><?php echo get_field('post_service_steps_title'); ?></h2>
            <div class="stages-wrap">
                <div class="stages wow fadeInUpShort"  data-wow-dalay="0.4s">
                    <?php foreach ( get_field('post_service_steps_list') as $item ) { ?>
                        <div class="stages__item">
                            <div class="stages__el">
                                <div class="stages__title"><?= $item['post_service_steps_item_title'] ?></div>
                                <div class="stages__text"><?= $item['post_service_steps_item_text'] ?></div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="head-decor"><?php echo get_field('post_service_steps_decor'); ?></div>
    </section>
    
    <div class="section-services-description section-virtues ">
        <div class="wrapper">
            <div class="worth-wrapper worth-wrapper_main">
                <div class="worth-description wow fadeInUpShort">
                    <div class="worth__description">
                        <h2 class="h2-decor mob-hide"><?php echo get_field('post_service_advantages_title'); ?></h2>
                        <div class="section-text worth__text worth__text-2 scroll-unvisible js-scroll">
                            <p><?php echo get_field('post_service_advantages_text'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="worth-img wow fadeInRigthShort" data-wow-delay="0.3s">
                    <div class="worth">
                        <div class="worth__item">
                            <div class="worth__card">
                                <div class="worth__picture with-decor">
                                    <div class="with-decor__inner">
                                        <h2 class="h2-decor mob-show"><?php echo get_field('post_service_advantages_title'); ?></h2>
                                        <div class="section-picture">
                                            <div class="section-picture__img js-lazy" data-src="<?php echo get_field('post_service_advantages_img'); ?>"></div>
                                        </div>
                                    </div>
                                    <div class="decor-right decor-worth decor-services decor-description decor-services-2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="section-compare section-compare-2">
        <div class="wrapper js-slider-group">
            <div class="h2-decor wow fadeInUpShort" data-wow-delay="0.3s"><?php echo get_field('post_service_bef-aft_title'); ?></div>
            <div class="compare-wrap">
                <div class="worth-wrapper js-slider-group">
                    <div class="worth-img compare__picture wow fadeInLeftShort">
                        <div class="worth js-slider-1">
                            <?php foreach ( get_field('post_service_bef-aft_list') as $item ) { ?>
                                <div class="worth__item">
                                    <div class="compare__picture with-decor">
                                        <div class="with-decor__inner">
                                            <div class="compare-img js-compare-img">
                                                <!-- The before image is first -->
                                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" data-src="<?= $item['post_service_bef-aft_item_img_before']; ?>" alt="" class="comparison-image js-img" />
                                                <!-- The after image is last -->
                                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" data-src="<?= $item['post_service_bef-aft_item_img_after']; ?>" alt="" class="comparison-image js-img" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="worth-description compare-description">

                        <div class="slider-nav slider-compare-nav scroll-unvisible js-slider-1_nav js-scroll-horizontal wow fadeInUpShort" data-wow-delay="0.6s">
                            <div class="slider-nav__slides js-horizontal">
                                <?php foreach ( get_field('post_service_bef-aft_list') as $item ) { ?>
                                    <div class="slider-nav__item js-slider-1_nav-item js-slide">
                                        <div class="slider-nav__card js-lazy" data-src="<?= $item['post_service_bef-aft_item_img_nav']; ?>"></div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="js-slider-1-1 wow fadeInUpShort" data-wow-delay="0.3s">
                            <?php foreach ( get_field('post_service_bef-aft_list') as $item ) { ?>
                                <div class="worth-description__item">
                                    <div class="compare__description">
                                        <div class="compare__title"><?= $item['post_service_bef-aft_item_title']; ?></div>
                                        <div class="compare__text scroll-unvisible js-scroll">
                                            <p><?= $item['post_service_bef-aft_item_txt']; ?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="decor-left decor-compare wow fadeInLeftShort"></div>
            </div>
        </div>
    </div>

    <?php if ( !empty ( get_field('post_service_specialists_list') ) ) { ?>
        <div class="section-specialist">
        <div class="wrapper">
            <div class="h2-decor wow fadeInRigthLeft"><?php echo get_field('post_service_specailists_title'); ?></div>
            <div class="specialist js-tabs-wrap">

                <div class="specialist--nav scroll-unvisible js-scroll-horizontal wow fadeInRigthShort"  data-wow-delay="0.3s">
                    <div class="tabs-nav specialist-nav js-horizontal">
                        <?php foreach ( get_field('post_service_specialists_list') as $item_spec ) { ?>
                            <div class="tabs-nav__link js-tab-link js-slide" data-href="#<?= $item_spec['specialists_item_ID'] ?>"><?= $item_spec['specialists_item_title_nav'] ?></div>
                        <?php } ?>
                    </div>
                </div>

                <div class="tabs-wrap specialist-tab wow fadeInUpShort" data-wow-delay="0.6s">
                    <?php foreach ( get_field('post_service_specialists_list') as $item_spec ) { ?>
                        <div class="tab js-tab" id="<?= $item_spec['specialists_item_ID'] ?>">
                            <div class="specialist-slider sliders-arrow js-slider-specialist">
                                <?php foreach ( $item_spec['specialists_item_doctors_list'] as $doc ) { ?>
                                    <div class="specialist-slider__item">
                                        <div class="specialist-slider__card" style="background: radial-gradient(100% 143.45% at 0% 52.01%, #fffffe 0%, #fff4e0 45.35%);">
                                            <div class="specialist-slider__head">
                                                <div class="specialist-slider__title"><?= $doc['specialists_item_doctors_item_name'] ?></div>
                                                <div class="specialist-slider__position"><?= $doc['specialists_item_doctors_item_position'] ?></div>
                                                <div class="specialist-slider__experience"><?= $doc['specialists_item_doctors_item_exp'] ?></div>
                                            </div>
                                            <div class="specialist-slider__img">
                                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
                                                     data-src="<?= $doc['specialists_item_doctors_item_photo'] ?>" alt="" class="js-img" /> <!-- 224*329-->
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <div class="specialist-decor wow fadeInUpShort" data-wow-delay="0.6s">
                <svg width="1120" height="313" viewBox="0 0 1120 313" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path opacity="0.5" d="M0 156.864C0 65.4545 78.1768 -6.40383 169.263 1.28156L1009.52 72.178C1071.98 77.4485 1120 129.688 1120 192.375C1120 258.995 1065.99 313 999.375 313H156.136C69.9043 313 0 243.096 0 156.864Z" fill="#E6E7EC" />
                </svg>
            </div>

        </div>
    </div>
    <?php } ?>
    
    <section class="section-price-direction section-price-page">

        <div class="wrapper">
            <h2 class="h2-decor wow fadeInLeftShort"><?php echo get_field('post_service_procedures_price_title'); ?></h2>
            <div class="faq-list wow fadeInUpShort" data-wow-dalay="0.4s">
                <div class="services-price">

                    <?php foreach ( get_field('post_service_procedures_price_list') as $item ) { ?>
                        <div class="services-price__item">
                            <div class="services-price__head">
                                <a class="services-price__title"><?= $item['post_service_procedures_price_item_title'] ?></a>
                                <?php if( $item['post_service_procedures_price_item_stock'] == true ) { ?>
                                    <a class="btn-2 services-price__action">Акция</a>
                                <?php } ?>
                            </div>
                            <div class="services-price__right">
                                <div class="services-price__counts">
                                    <?php if ( !empty( $item['post_service_procedures_price_item_old_price'] ) ) { ?>
                                        <span class="services-price__counts-currency services-price__new"><?= $item['post_service_procedures_price_item_price'] ?></span>
                                        <span class="services-price__old-price"><?= $item['post_service_procedures_price_item_old_price'] ?></span>
                                    <?php } else { ?>
                                        <span class="services-price__counts-currency"><?= $item['post_service_procedures_price_item_price'] ?></span>
                                    <?php } ?>
                                </div>
                                <div class="services-price__btn">
                                    <span data-src="#order" class="btn btn_service-order js-fancybox">заказать</span>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                </div>
            </div>
        </div>

    </section>

    <section class="form-order-inline">
        <div class="wrapper">
            <h2 class="h2-decor wow fadeInLeftShort">записаться</h2>
            <div class="form-wrap wow fadeInUpShort" data-wow-dalay="0.4s">
                <div class="form-inline">

                    <?php echo do_shortcode('[contact-form-7 id="333" title="Заказ услуги"]'); ?>

                </div>
                <div class="decor-right decor-form js-lazy" data-src="<?php bloginfo('template_url') ?>/img/examples/form-bg.jpg"></div>
            </div>
        </div>
    </section>

</main>

<?php get_footer(); ?>

<script>
    var breadFirstItem = $('.breadcrumb .breadcrumb-item:first-child');
    var categoryItem = '<li class="breadcrumb-item" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a class="breadcrumbs__link" href="/category/" itemprop="item"><span itemprop="name">Услуги</span></a><meta itemprop="position"></li>';
    breadFirstItem.after(categoryItem);
</script>

<script>
    $(document).ready(function () {
        var title  = $('h1').text();
        console.log(title);
        $('select.wpcf7-select option').each(function () {
            if ( $(this).text() == title ) {
                $(this).attr('selected', '');
            }
        });
    });
</script>