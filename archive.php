<?php get_header(); ?>

<?php

$term = get_queried_object();

$secondary_categories = get_categories( array(
    'parent'     => $term->term_id,
    'hide_empty' => 0
) );

?>

<main class="content">

    <section class="head-inner">
        <div class="wrapper wrapper-inner-head">

            <!--    Breadcrumbs -->
            <?php if ( function_exists( 'breadcrumbs' ) ) breadcrumbs(); ?>
            <!--    End Breadcrumbs -->

            <div class="head-inner__heading">
                <h1 class="h2-decor h1-inner"><?php echo str_replace("Рубрика: ", "", get_the_archive_title()); ?></h1>
                <div class="section-text"><?php echo get_the_archive_description(); ?></div>
            </div>

            <div class="head-inner__services">
                <?php if ( !empty($secondary_categories) ) { ?>
                    <div class="list-decor list-services">
                        <ul>
                            <?php foreach ( $secondary_categories as $s_cat ) { ?>
                                <li>
                                    <a href="<?= $s_cat -> slug ?>"><?= $s_cat -> name ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>
            </div>

        </div>

        <?php if ( !empty( get_field('p_archive_desc_bg_img', $term) ) ) { ?>
            <div class="bg-head js-bg" data-preload="<?php echo get_field('p_archive_desc_bg_img', $term); ?>"></div>
        <?php } else { ?>
            <div class="bg-head js-bg" data-preload="<?php bloginfo('template_url') ?>/img/examples/direction-bg.jpg"></div>
        <?php } ?>
        
    </section>

    <div class="section-services section-services_inner">
        <div class="wrapper">

            <h2 class="h2-decor wow fadeInLeftShort">Услуги</h2>

            <div class="services-main services-main_inner">
                <?php
                global $post;
                $term = get_queried_object();
                $mypost_Query = new WP_Query ( array(
                    'category_name'    => $term->name,
                    'post_type'        => 'post',
                    'post_status'      => 'publish',
                    'posts_per_page'   => -1,
                ) );
                if ( $mypost_Query->have_posts() ) {
                while ( $mypost_Query->have_posts() ) { $mypost_Query->the_post(); ?>

                <?php
                    $price       = get_field ( 'post_service_price', $post );
                    $description = get_field ( 'post_service_tit_txt', $post );
                ?>

                    <div class="services-main__item  wow fadeInUpShort" data-wow-delay="0.3s">
                        <div class="services-main__card">
                            <a href="<?php echo get_permalink(); ?>" class="services-main__picture">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
                                     data-src="<?php echo get_the_post_thumbnail_url(); ?>"
                                     alt=""
                                     class="js-lazy" />
                            </a>
                            <div class="services-main__head">
                                <span><?= $post->post_title ?></span>
                                <div class="services-main__price"><?= $price ?></div>
                            </div>
                            <div class="services-main__list">
                                <div class="services-main__head"><a href="<?php echo get_permalink(); ?>"><?= $post->post_title ?></a>
                                </div>
                                <div class="services-main__text">
                                    <p><?= $description ?></p>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php }
                }
                else { echo('<p>Извините, нет услуг.</p>'); }
                wp_reset_postdata();
                ?>

            </div>

        </div>
    </div>

    <div class="section-services-description">
        <div class="wrapper">
            <div class="worth-wrapper worth-wrapper_main">
                <div class="worth-description wow fadeInUpShort">
                    <div class="worth__description">
                        <h2 class="h2-decor mob-hide"><?php echo get_field('p_archive_desc_title', $term); ?></h2>
                        <div class="section-text worth__text worth__text-2 scroll-unvisible js-scroll">
                            <p><?php echo get_field('p_archive_desc_txt', $term); ?></p>
                        </div>
                    </div>
                </div>
                <div class="worth-img wow fadeInRigthShort" data-wow-delay="0.3s">
                    <div class="worth">
                        <div class="worth__item">
                            <div class="worth__card">
                                <div class="worth__picture with-decor">
                                    <div class="with-decor__inner">
                                        <h2 class="h2-decor mob-show"><?php echo get_field('p_archive_desc_title', $term); ?></h2>
                                        <div class="section-picture">
                                            <div class="section-picture__img js-lazy" data-src="<?php echo get_field('p_archive_desc_img', $term); ?>"></div>
                                        </div>
                                    </div>
                                    <div class="decor-right decor-worth decor-services js-lazy" data-src="<?php echo get_field('p_archive_desc_decor', $term); ?>"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if ( !empty( get_field('p_archive_excellence_list', $term) ) ) { ?>
        <div class="section-excellence">
            <div class="wrapper">

                <div class="h2-decor wow fadeInLeftShort"><?php echo get_field('p_archive_excellence_title'); ?></div>

                <div class="excellence-wrap wow fadeInUpShort" data-wow-delay=".3s">
                    <div class="excellence">
                        <?php foreach ( get_field('p_archive_excellence_list', $term) as $item ) { ?>
                            <div class="excellence__item">
                                <div class="excellence-slider js-slider-2">
                                    <?php foreach ( $item['p_archive_excellence_item_imgs_list'] as $itemImage ) { ?>
                                        <div class="excellence-slider__item">
                                            <a href="<?= $itemImage['p_archive_excellence_item_imgs_item'] ?>"
                                               class="excellence-slider__el js-fancybox js-bg"
                                               data-fancybox="license"
                                               data-preload="<?= $itemImage['p_archive_excellence_item_imgs_item'] ?>"></a>
                                        </div>
                                    <?php } ?>
                                </div>

                                <div class="excellence__title"><?= $item['p_archive_excellence_item_title'] ?></div>

                                <span class="excellence__icon">
                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
                                         data-src="<?= $item['p_archive_excellence_item_icon'] ?>"
                                         alt=""
                                         class="js-lazy" />
                                </span>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

    <?php } ?>

    <?php if ( !empty (get_field('p_archive_specialists_list', $term) ) ) { ?>
        <div class="section-specialist section-specialist_3">
            <div class="wrapper">

                <div class="h2-decor wow fadeInRigthLeft"><?php echo get_field('p_archive_specailists_title', $term); ?></div>

                <div class="specialist js-tabs-wrap">

                    <div class="specialist--nav scroll-unvisible js-scroll-horizontal wow fadeInRigthShort"  data-wow-delay="0.3s">

                        <div class="tabs-nav specialist-nav js-horizontal">
                            <?php foreach ( get_field('p_archive_specialists_list', $term) as $item_spec ) { ?>
                                <div class="tabs-nav__link js-tab-link js-slide" data-href="#<?= $item_spec['specialists_item_ID'] ?>"><?= $item_spec['specialists_item_title_nav'] ?></div>
                            <?php } ?>
                        </div>

                    </div>

                    <div class="tabs-wrap specialist-tab wow fadeInUpShort" data-wow-delay="0.6s">
                        <?php foreach ( get_field('p_archive_specialists_list', $term) as $item_spec ) { ?>
                            <div class="tab js-tab" id="<?= $item_spec['specialists_item_ID'] ?>">
                                <div class="specialist-slider sliders-arrow js-slider-specialist">
                                    <?php foreach ( $item_spec['specialists_item_doctors_list'] as $doc ) { ?>
                                        <div class="specialist-slider__item">
                                            <div class="specialist-slider__card" style="background: radial-gradient(100% 143.45% at 0% 52.01%, #fffffe 0%, #fff4e0 45.35%);">
                                                <div class="specialist-slider__head">
                                                    <div class="specialist-slider__title"><?= $doc['specialists_item_doctors_item_name'] ?></div>
                                                    <div class="specialist-slider__position"><?= $doc['specialists_item_doctors_item_position'] ?></div>
                                                    <div class="specialist-slider__experience"><?= $doc['specialists_item_doctors_item_exp'] ?></div>
                                                </div>
                                                <div class="specialist-slider__img">
                                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
                                                         data-src="<?= $doc['specialists_item_doctors_item_photo'] ?>" alt="" class="js-img" /> <!-- 224*329-->
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="specialist-decor wow fadeInUpShort" data-wow-delay="0.6s">
                    <svg width="1120" height="313" viewBox="0 0 1120 313" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path opacity="0.5" d="M0 156.864C0 65.4545 78.1768 -6.40383 169.263 1.28156L1009.52 72.178C1071.98 77.4485 1120 129.688 1120 192.375C1120 258.995 1065.99 313 999.375 313H156.136C69.9043 313 0 243.096 0 156.864Z" fill="#E6E7EC" />
                    </svg>
                </div>

            </div>
        </div>
    <?php } ?>


    <?php if ( !empty($secondary_categories) ) { ?>
        <section class="section-price-direction">
            <div class="wrapper">

                <h2 class="h2-decor wow fadeInLeftShort">прайс-лист направления</h2>

                <div class="faq-list">
                    <?php foreach ( $secondary_categories as $s_cat ) { ?>
                        <div class="faq-list__item js-accordion-wrap wow fadeInUpShort"  data-wow-delay="0.2s">
                            <div class="faq-list__title js-accordion-head">
                                <span><?= $s_cat -> name ?></span>
                                <span class="faq-list__icon">
                                    <i class="icon-plus"></i>
                                </span>
                            </div>
                            <div class="faq-list__info js-accordion-body">
                                <div class="services-price">

                                    <?php
                                    global $post;
                                    $categorie = $s_cat -> name;
                                    $mypost_Query = new WP_Query ( array(
                                        'category_name'    => $categorie,
                                        'post_type'        => 'post',
                                        'post_status'      => 'publish',
                                        'posts_per_page'   => 3,
                                    ) );
                                    if ( $mypost_Query->have_posts() ) {
                                    while ( $mypost_Query->have_posts() ) { $mypost_Query->the_post(); ?>
                                    <?php
                                        $price     = get_field ( 'post_service_price', $post );
                                        $old_price = get_field ( 'post_service_old_price', $post );
                                        $stock     = get_field ( 'post_service_stock', $post );
                                    ?>
                                    <div class="services-price__item">
                                        <div class="services-price__head">
                                            <a href="<?php echo get_permalink(); ?>"
                                               class="services-price__title"><?= $post->post_title ?></a>
                                            <?php if ($stock == true) { ?>
                                                <a class="btn-2 services-price__action">Акция</a>
                                            <?php } ?>
                                        </div>
                                        <div class="services-price__right">
                                            <div class="services-price__counts">
                                                <?php if ( !empty( $old_price ) ) { ?>
                                                    <span class="services-price__counts-currency services-price__new"><?= $price ?></span>
                                                    <span class="services-price__old-price"><?= $old_price ?></span>
                                                <?php } else { ?>
                                                    <span class="services-price__counts-currency"><?= $price ?></span>
                                                <?php } ?>
                                            </div>
                                            <div class="services-price__btn">
                                                <span data-src="#order" class="btn btn_service-order js-fancybox">заказать</span>
                                            </div>
                                        </div>
                                    </div>

                                    <?php }
                                    }
                                    else { echo('<p>Извините, нет услуг.</p>'); }
                                    wp_reset_postdata();
                                    ?>

                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>

            </div>
        </section>
    <?php } ?>

</main>

<?php get_footer(); ?>

<script>
    var breadFirstItem = $('.breadcrumb .breadcrumb-item:first-child');
    var categoryItem = '<li class="breadcrumb-item" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a class="breadcrumbs__link" href="/category/" itemprop="item"><span itemprop="name">Услуги</span></a><meta itemprop="position"></li>';
    breadFirstItem.after(categoryItem);
</script>

<script>
    $('.services-price__item').each(function () {
        var title  = $(this).find('.services-price__title').text();
        var btn    = $(this).find('.btn_service-order');

        btn.on('click', function () {
            var select = $('#order .jq-selectbox__select-text');
            select.text(title);

            $('.jq-selectbox__dropdown ul li').each(function () {
                if ( $(this).text() == select.text() ) {
                    $(this).addClass('selected sel');
                } else {
                    $(this).removeClass('selected sel');
                }
            });

            $('select.wpcf7-select option').each(function () {
                if ( $(this).text() == select.text() ) {
                    $(this).attr('selected', '');
                }
            });
        });
    });
</script>
