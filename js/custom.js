
var $win = $(window),
    $body = $('body'),
    $header = $('#header'),
    reloadBg, reloadImg, loadJS;

$.extend({
    getManyCss: function (urls, callback, nocache) {
        if (typeof nocache == 'undefined') nocache = false; // default don't refresh
        $.when.apply($,
            $.map(urls, function (url) {
                if (nocache) url += '?_ts=' + new Date().getTime(); // refresh? 
                return $.get(url, function () {
                    $('<link>', { rel: 'preload stylesheet', 'as': 'style', 'href': url, 'crossorigin': 'anonymous' }).appendTo('head');
                });
            })
        ).then(function () {
            if (typeof callback == 'function') callback();
        });
    },
});

loadJS = function (url, implementationCode, location) {
                var scriptTag = document.createElement('script');
                scriptTag.src = url;

                scriptTag.defer = true;

                scriptTag.onload = implementationCode;
                scriptTag.onreadystatechange = implementationCode;

                location.appendChild(scriptTag);
            };

$(function () {

    

    

})





$win.on('load', function () {
    
    
    function initSlider() {
        $('.js-slider-main').each(function () {
            var $el = $(this);
            $el.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                infinite: true,
                speed: 500,
                fade: true,
                cssEase: 'linear',
                autoplay: true,
                autoplaySpeed: 5000,
                pauseOnHover: false,
                pauseOnFocus: true,
                arrows: true,
                speed: 500,
                appendArrows: '.js-append-main-arrow',
                prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-arrow-prev"></i></button>',
                nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-arrow-next"></i></button>'
            });
        });
        if ($('.js-slider-4').length) {
            $('.js-slider-4').slick({
                slidesToShow: 3,
                slidesToScroll: 3,
                speed: 600,
                arrows: true,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 5000,
                pauseOnHover: true,
                speed: 300,
                prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-arrow-prev"></i></button>',
                nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-arrow-next"></i></button>',
                responsive: [{
                    breakpoint: 960,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                }, {
                    breakpoint: 559,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }]
            });

            $('.js-slider-4').live('mouseenter mousemove touchstart', function () {
                $(this).slick('slickPause');
            })
        }
        if ($('.js-slider-5').length) {
            $('.js-slider-5').slick({
                slidesToShow: 4,
                slidesToScroll: 4,
                speed: 600,
                arrows: true,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 5000,
                pauseOnHover: true,
                speed: 300,
                prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-arrow-prev"></i></button>',
                nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-arrow-next"></i></button>',
                responsive: [{
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                }, {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                }, {
                    breakpoint: 479,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }]
            });

            $('.js-slider-5').live('mouseenter mousemove touchstart', function () {
                $(this).slick('slickPause');
            })
        }



        $('.js-slider-1').each(function () {
            var $el = $(this),
                $navSlider = $el.closest('.js-slider-group').find('.js-slider-1_nav'),
                $el2 = $el.closest('.js-slider-group').find('.js-slider-1-1');
            var $touchMove = true;
            if ($el.closest('.compare__picture').length) {
                $touchMove = false;
            }
            $el.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                arrows: false,
                asNavFor: $el2,
                pauseOnHover: true,
                swipe: $touchMove,
                //autoplay: true,

                autoplaySpeed: 5000,
                responsive: [{
                    breakpoint: 767,
                    settings: {
                        speed: 300,
                        adaptiveHeight: true
                    }
                }]
            });
            $el.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                $navSlider.find('.js-slider-1_nav-item').eq(nextSlide).addClass('active').siblings('.js-slider-1_nav-item').removeClass('active');
            });
            $win.scroll(function () {
                var $offsetStart = $el.offset().top - window.innerHeight;
                var $offsetFinish = $el.closest('.js-slider-group').find('.js-slider-1-1').offset().top + $el.closest('.js-slider-group').find('.js-slider-1-1').outerHeight() / 3 * 2;
                if ($win.scrollTop() >= $offsetStart && $win.scrollTop() <= $offsetFinish) {
                    if (!$el.is('.scrolled')) {

                        $el.addClass('scrolled')
                        $el.slick('slickPlay');
                        setTimeout(function () {

                            $el.slick("slickNext");
                        }, 1500)
                    }
                }

                else {
                    $el.slick('slickPause');
                    $el.removeClass('scrolled')
                }
            })
            $('.js-slider-1').live('mouseenter mousemove touchstart', function () {

                $(this).slick('slickPause');
                $(this).addClass('scrolled')
            })
            $('.js-slider-1_nav, .js-slider-1-1').live('mouseenter mousemove touchstart', function () {

                $(this).closest('.js-slider-group').find('.js-slider-1').slick('slickPause');
                $(this).closest('.js-slider-group').find('.js-slider-1').addClass('scrolled')
            })
        })

        if ($('.js-slider-3').length > 0) {

            $('.js-slider-3').slick({
                slidesPerRow: 1,
                rows: 2,
                arrows: false,
                fade: true,
                arrows: true,
                //autoplay: true,
                autoplaySpeed: 8000,
                prevArrow: '<button type="button" class="slick-arrow slick-arrow-2 slick-prev"><i class="icon-arrow-prev"></i></button>',
                nextArrow: '<button type="button" class="slick-arrow slick-arrow-2 slick-next"><i class="icon-arrow-next"></i></button>',

            });
        }

        $('.compare__picture .js-slider-1').on('mouseenter touchstart', function () {

            $('.js-slider-1').slick('slickPause');
        })
            .on('mouseleave touchend', function () {
                $('.js-slider-1').slick('slickPlay');
            })
        $('.js-slider-1-1').each(function () {
            var $el = $(this),
                $navSlider = $el.closest('.js-slider-group').find('.js-slider-1_nav'),
                $el2 = $el.closest('.js-slider-group').find('.js-slider-1')
            var $touchMove = true;
            if ($el.closest('.compare__picture').length) {
                $touchMove = false;
            }
            $el.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                arrows: false,
                touchMove: $touchMove,
                swipeToSlide: $touchMove,
                asNavFor: $el2,
                responsive: [{
                    breakpoint: 767,
                    settings: {
                        adaptiveHeight: true,
                        speed: 250
                    }
                }]
            });
            $el.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                $navSlider.find('.js-slider-1_nav-item').eq(nextSlide).addClass('active').siblings('.js-slider-1_nav-item').removeClass('active');
            });
        })

        $('.js-slider-1_nav-item').on('click', function () {
            var $el = $(this);
            $el.addClass('active').siblings('.js-slider-1_nav-item').removeClass('active');
            $el.closest('.js-slider-group').find('.js-slider-1').slick('slickGoTo', $el.index());
            return false;
        })
            .on('mouseenter', function () {
                $('.js-slider-1').slick('slickPause');
            })
            .on('mouseleave', function () {
                $('.js-slider-1').slick('slickPlay');
            })

        if ($('.js-slider-1_nav-item:first-child').length) {
            $('.js-slider-1_nav-item:first-child').addClass('active')
        }
        $('.js-slider-reviews').each(function () {
            var $el = $(this)
            $el.slick({
                speed: 600,
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: true,
                infinite: true,
                //autoplay: true,
                autoplaySpeed: 5000,
                speed: 300,
                prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-arrow-prev"></i></button>',
                nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-arrow-next"></i></button>',
                responsive: [{
                    breakpoint: 767,
                    settings: {
                        adaptiveHeight: true,
                        slidesToShow: 1
                    }
                }]
            });

            $win.scroll(function () {
                var $offsetStart = $el.offset().top - window.innerHeight;
                var $offsetFinish = $offsetStart + $el.outerHeight();
                if ($win.scrollTop() >= $offsetStart && $win.scrollTop() <= $offsetFinish) {
                    if (!$el.is('.scrolled')) {

                        $el.addClass('scrolled')
                        $el.slick('slickPlay');
                        setTimeout(function () {

                            $el.slick("slickNext");
                        }, 1500)
                    }
                }

                else {
                    $el.slick('slickPause');
                    $el.removeClass('scrolled')
                }
            })
        })

        $('.js-slider-reviews').live('mouseenter mousemove touchstart', function () {

            $(this).slick('slickPause');
            $(this).addClass('scrolled')
        })
        if ($('.js-slider-specialist').length) {
            $('.js-slider-specialist').slick({
                speed: 600,
                variableWidth: true,
                arrows: true,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 5000,
                pauseOnHover: true,
                speed: 300,
                prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-arrow-prev"></i></button>',
                nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-arrow-next"></i></button>'
            });
        }



        if ($('.js-slider-equipment-1').length) {

            $('.js-slider-equipment-1').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                infinite: false,
                arrows: false,

                focusOnSelect: true
            });
            $('.js-slider-equipment-1').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                $('.js-slider-equipment_nav').find('.js-slider-equipment_nav-item').eq(nextSlide).addClass('active').siblings('.js-slider-equipment_nav-item').removeClass('active');

            });
        }


        if ($('.js-slide-1').length) {

            $('.js-slide-1').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                infinite: false,
                arrows: false,

                focusOnSelect: true,
                asNavFor: '.js-slide-1_text'
            });
            $('.js-slide-1').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                $('.js-slide-1_navs').find('.js-slide-1_nav').eq(nextSlide).addClass('active').siblings('.js-slide-1_nav').removeClass('active');

            });
        }
        if ($('.js-slide-1_text').length) {

            $('.js-slide-1_text').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                infinite: false,
                arrows: false,
                adaptiveHeight: true,
                focusOnSelect: true,
                asNavFor: '.js-slide-1',
                responsive: [{
                    breakpoint: 767,
                    settings: {
                        speed: 300,
                        adaptiveHeight: true
                    }
                }]
            });
            $('.js-slide-1').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                $('.js-slide-1_navs').find('.js-slide-1_nav').eq(nextSlide).addClass('active').siblings('.js-slide-1_nav').removeClass('active');

            });
        }


        $('.js-slide-1_nav').on('click', function () {
            var $el = $(this);
            $el.addClass('active').siblings('.js-slide-1_nav').removeClass('active')
            $el.closest('.js-slider-group').find('.js-slide-1').slick('slickGoTo', $el.index())
        }).on('mouseenter', function () {
            $('.js-slide-1').slick('slickPause')
        }).on('mouseleave', function () {
            $('.js-slide-1').slick('slickPlay')
        })

        if ($('.js-slider-2').length > 0) {
            $('.js-slider-2').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                speed: 500,
                fade: true,
                cssEase: 'linear',
                autoplay: true,
                autoplaySpeed: 5000,
                pauseOnHover: false,
                pauseOnFocus: true,
                arrows: true,
                speed: 500,
                prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-arrow-prev"></i></button>',
                nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-arrow-next"></i></button>'
            });
        }

        $('.js-slider-equipment_nav-item').on('click', function () {
            var $el = $(this);
            $el.addClass('active').siblings('.js-slider-equipment_nav-item').removeClass('active')
            $el.closest('.js-slider-group').find('.js-slider-equipment-1').slick('slickGoTo', $el.index())
        }).on('mouseenter', function () {
            $('.js-slider-equipment-1').slick('slickPause')
        }).on('mouseleave', function () {
            $('.js-slider-equipment-1').slick('slickPlay')
        })


    }
    var $time = 0
    if ($body.is('.ios')) {
        $time = 2300;
    }


    var onCompareCallback = function () {
        $(".js-compare-img").each(function () {

            $(this).twentytwenty({
                default_offset_pct: 0.5,
                orientation: 'horizontal',
                no_overlay: true,
                move_slider_on_hover: true,
                move_with_handle_only: true,
                click_to_move: true
            });

        })
    }
    var onloadCallback = function () {

        if ($('.js-horizontal').length) {

            function draggableScroll() {
                $('.js-horizontal').each(function () {
                    var $width = 0
                    $(this).find('.js-slide').each(function () {

                        var $el = $(this);
                        $width = $width + $el.outerWidth() + parseFloat($el.css('margin-left')) + parseFloat($el.css('margin-right'))
                    })
                    $(this).css('width', $width);
                    if ($(this).is('.equipment-nav') && window.innerWidth > 1023) {
                        $(this).css('width', '');
                    }
                })

                $('.js-horizontal').each(function () {

                    var $carousel = $(this),
                        $view = $carousel.closest('.js-scroll-horizontal'),
                        $item = $carousel.find(".js-slide");

                    var offsetScroll = $view.outerWidth() - $carousel.outerWidth()
                    if (offsetScroll > 0) {
                        offsetScroll = 0
                    }
                    var draggable = new Draggable($carousel, {
                        type: "x",
                        minimumMovement: 30,
                        throwProps: true,
                        scrollSensitivity: 1,
                        scrollSpeed: 2000,
                        bounds: {
                            maxX: 0,
                            minX: offsetScroll
                        }
                    });


                })
            }
            draggableScroll()
            $win.resize(function () {

                draggableScroll()
            })

        }
    }
    var cssfiles2 = ['/wp-content/themes/reverse/css/style_new.css'];

    var cssfiles = ['/wp-content/themes/reverse/css/style.css'];
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        $body.addClass('ios');
        
        setTimeout(function () {
            $body.removeClass('loaded');
        }, 3500);
        setTimeout(function () {

            $.getManyCss(cssfiles, function () {
                reloadBg();
                setTimeout(function () {

                    if ($('.js-compare-img').length) {
                        loadJS('/wp-content/themes/reverse/js/components/jquery.twentytwenty.js', onCompareCallback, document.body);

                    }

                    if ($('.js-horizontal').length) {
                        loadJS('/wp-content/themes/reverse/js/components/TweenMax.min.js', onloadCallback, document.body);
                    }
                }, 500)

                $.getManyCss(cssfiles2, function () { });
            });
                        

        }, 2000);
        
        setTimeout(function () {
            loadJS('/wp-content/themes/reverse/js/components/slick.js', initSlider, document.body);
        }, 2000);

    } else {
        $body.addClass('web');
        setTimeout(function () {
            $body.removeClass('loaded');
        }, 1000);
        setTimeout(function () {

            $.getManyCss(cssfiles, function () {


                reloadBg();

                if ($('.js-compare-img').length) {
                    loadJS('/wp-content/themes/reverse/js/components/jquery.twentytwenty.js', onCompareCallback, document.body);

                }

                if ($('.js-horizontal').length) {
                    loadJS('/wp-content/themes/reverse/js/components/TweenMax.min.js', onloadCallback, document.body);
                }
                $.getManyCss(cssfiles2, function () { });

            });
            
            loadJS('/wp-content/themes/reverse/js/components/slick.js', initSlider, document.body);

        }, 400);

    }
    setTimeout(function () {
        $("head").append('<link rel="preload stylesheet" href="/wp-content/themes/reverse/css/fonts.css" as="style">');
    }, 3500);












    setTimeout(function () {

        $('#loader').remove()
    }, 3000)
    reloadImg = function () {
        $(".js-img:not('.loaded')").each(function () {
            var $el = $(this);
            $el.attr('src', $el.data("src")).addClass('loaded');
        });
    }
    reloadImg()
    reloadBg = function () {

        $(".js-bg:not(.loaded)").each(function () {
            var $el = $(this);
            $el.css('background-image', 'url(' + $el.data("preload") + ')').addClass('loaded');
        });
    }




    if ($('.js-slider-1_nav-item:first-child').length) {
        $('.js-slider-1_nav-item:first-child').addClass('active')
    }





    $('.js-anchor').on('click', function () {
        var $offset = $($(this).attr('data-href')).offset().top
        $('html, body').animate({
            scrollTop: $offset
        }, 500);
        return false;
    })






    var y_offsetWhenScrollDisabled,
        offset;
    var $topArrow = $("#arrowScroll");

    var $flagFix = false;
    var $flagFixO = false
    $win.scroll(function () {
        y_offsetWhenScrollDisabled = $win.scrollTop();
        if (y_offsetWhenScrollDisabled > 0) {
            $('#header').addClass('fixed');
            $flagFix = true;
        } else {
            if (!$body.is('.open-header')) {
                $('#header').removeClass('fixed');
                $flagFix = false;

            }
        }
        if ($topArrow.length) {
            if (y_offsetWhenScrollDisabled >= window.innerHeight) {
                $topArrow.addClass('arr-up')
            } else {
                $topArrow.removeClass('arr-up')
            }
        }
    })


    function lockScroll() {
        if ($flagFix) {
            $flagFixO = true;
        }
        offset = y_offsetWhenScrollDisabled;
        $body.addClass('scrollDisabled');
        $('html').css('margin-top', -y_offsetWhenScrollDisabled);
        if ($flagFixO) {
            setTimeout(function () {
                $('#header').addClass('fixed')
            }, 50);

        }
    }

    function unlockScroll() {
        $body.removeClass('scrollDisabled');
        $('html').css('margin-top', 0);
        $('html, body').animate({
            scrollTop: offset
        }, 0);
    }




    if ($('.js-slide-1_nav:first-child').length) {
        $('.js-slide-1_nav:first-child').addClass('active')
    }


    /* accordion packets */
    $('.js-accordion-head').on('click', function () {
        var $el = $(this);
        $el.closest('.js-accordion-wrap').toggleClass('active').find('.js-accordion-body').slideToggle(200);
        if ($el.closest('.js-accordion-wrap').is('.active')) {
            $el.closest('.js-accordion-wrap').siblings('.js-accordion-wrap').removeClass('active').find('.js-accordion-body').slideUp(200);
        }
    });
    /* accordion packets */



    if ($('.js-slider-equipment_nav-item:first-child').length) {
        $('.js-slider-equipment_nav-item:first-child').addClass('active')
    }
    // tabs
    $('.js-tab-link').on('click', function () {
        var $el = $(this),
            $href = $el.attr('data-href'),
            $parent = $el.closest('.js-tabs-wrap');
        $el.siblings('.js-tab-link').removeClass('active');
        $el.addClass('active');
        $parent.find('.js-tab').removeClass('active');
        $($href).addClass('active');
        if ($($href).find(".js-bg:not(.loaded)").length) {

            reloadImg()
        }
        if ($($href).find(".js-img:not(.loaded)").length) {

            reloadBg()
        }
        return false;
    });
    // tabs
    $('.js-button-nav').click(function () {
        var $el = $(this);
        $el.toggleClass('active');
        $body.toggleClass('open-header');
        $('#mainNav').toggleClass('active');
        $('.js-overlay').toggleClass('overlay');
        if ($body.is('.open-header')) {
            lockScroll();
        } else {
            setTimeout(function () {
                unlockScroll();
            }, 400);
        }
        $('.has-children.opened').each(function () {
            $(this).removeClass('opened').find("ul").css('display', '');

        })
        return false;
    });


    $('.js-open-dropdown').on('click', function () {
        var $flagOpen = false;
        if ($(this).closest('.js-dropdown').is('.open')) {
            $flagOpen = true;
        }
        $('.js-dropdown').removeClass('open')
        if ($flagOpen == false) {
            $(this).closest('.js-dropdown').addClass('open');
        }
        return false
    })


    $(document).on('touchstart click', function (e) {
        if ($('.js-dropdown').is('.open')) {
            if ($(e.target).closest('.js-dropdown').length > 0) {
                return;
            }
            $('.js-dropdown.open').find('.js-open-dropdown').trigger('click');

            return false;
        }

    });

    $('.js-link-nav').on('click', function () {
        if (window.innerWidth > 1199) {

            var $el = $(this);
            $el.toggleClass('active');
            $body.toggleClass('open-header');

            $el.closest('.js-header-nav').toggleClass('open')
            if ($body.is('.open-header')) {
                lockScroll();
                $('#overlayHeader').fadeIn();
            } else {
                $('#overlayHeader').fadeOut();
                setTimeout(function () {
                    unlockScroll();
                }, 400);
            }
        }
        return false;
    });
    $('.js-link-nav-service').on('click', function () {
        if (window.innerWidth > 1199) {
            var $el = $(this);
            $el.toggleClass('active');
            $body.toggleClass('open-header');
            $el.closest('.js-header-nav').toggleClass('open')
            if ($body.is('.open-header')) {
                lockScroll();
                $('#overlayHeader').fadeIn();
            } else {
                $('#overlayHeader').fadeOut();
                setTimeout(function () {
                    unlockScroll();
                }, 400);
            }
        }
        return false;
    });


    // navigation
    // catalog nav
    function widthMenu() {
        $('.menu-list').each(function () {
            if (window.innerWidth > 1199) {
                var widthFix1 = window.innerWidth;
                var widthFix2 = $(this).closest('.header-nav').offset().left + parseFloat($(this).css('left'));
                var widthFix3 = widthFix1 - widthFix2;
                $(this).css('width', widthFix3 + "px");
            } else {
                $(this).css('width', '');
            }
        });
    }

    widthMenu()
    $win.on('resize', function () {
        widthMenu()
    });

    $('.js-menu-wrapper > ul > li')
        .live("mouseenter", function () {
            if (window.innerWidth > 1199) {
                var $el = $(this),
                    $url = $el.children('a').attr('data-img');
                if (
                    $el.closest('li').is('.has-children')) {
                    $el.closest('.js-menu-wrapper').find('.has-children').find('ul').hide()
                    $el.closest('.js-menu-wrapper').find('li').removeClass('hovered')
                    $el.addClass('hovered').find('ul').css('display', 'block')
                    $el.closest('.js-header-nav').addClass('cat-hover')
                } else {

                    $el.closest('.js-header-nav').removeClass('cat-hover')
                }
                $el.closest('.js-menu-wrapper').find('.js-img-menu').css('background-image', 'url(' + $url + ')').addClass('shown')
            }
        })
        .live("mouseleave", function () {
            var $el = $(this),
                $url = $el.children('a').attr('data-img');
            $el.closest('.js-header-nav').removeClass('cat-hover')
            $el.removeClass('hovered').find('ul').css('display', '')
            $el.closest('.js-menu-wrapper').find('.js-img-menu').removeClass('shown');
            $el.removeClass('opened')
        });

    $('.js-menu-wrapper li li')
        .live("mouseenter", function () {
            if (window.innerWidth > 1199) {

                var $el = $(this),
                    $url = $el.children('a').attr('data-img');
                $el.addClass('hovered')
                $el.siblings().removeClass('hovered')
                $el.closest('.js-menu-wrapper').find('.js-img-menu').css('background-image', 'url(' + $url + ')').addClass('shown')
            }
        });

    $('.js-arr-menu')
        .on("click", function () {
            var $el = $(this).closest('.has-children').find('ul');
            $el.slideToggle()
            $el.closest('li').toggleClass('opened');
        })

    $('#overlayHeader').on('click', function () {
        $('.js-link-nav').filter('.active').trigger('click');
        $('.js-link-nav-service').filter('.active').trigger('click')
    });

    // navigation



    // scroll top
    if ($topArrow.length) {
        /* arrow up*/
        $topArrow.on('mouseenter click', function () {
            var $el = $(this);
            if ($el.is('.arr-up')) {

                $('html, body').stop().animate({
                    scrollTop: 0
                }, 500);
            } else {

                $('html, body').stop().animate({
                    scrollTop: $('.content > *:first-child').offset().top + $('.content > *:first-child').outerHeight()
                }, 500);
            }
        });

    }
    // scroll top




    if ($('.js-styled').length) {
        $('.js-styled').styler({
            selectSearchLimit: 3,
            locale: 'ru'
        });

    }


    if ($('.js-datepicker').length) {
        var dateToday = new Date();
        $('.js-datepicker').datepicker({
            numberOfMonths: 1,
            closeText: 'Закрыть',
            prevText: '',
            minDate: dateToday,
            currentText: 'Сегодня',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
            ],
            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
                'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'
            ],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekHeader: 'Не',
            autoClose: true,
            firstDay: 1,
            dateFormat: 'dd.mm.yy',
            changeMonth: true,
            changeYear: true,
            onSelect: function (selectedDate) {

                if ($(this).is('.error')) {
                    $(this).removeClass('error').next('.error').hide()
                }
            }
        });
    }

    $('.js-step-next').on('click', function () {
        var $form = $(this).closest('.js-tab'),
            $index = $form.find('.js-step').filter('.active').index() + 1,
            $counts = $form.find('.js-step').length
        $form.find('.js-step').filter('.active').next('.js-step').addClass('active').siblings('.js-step').removeClass('active');
        $form.find('.js-step-nav-item').eq($index).addClass('active').siblings('.js-step-nav-item').removeClass('active');
        for (var i = 0; i < $index; i++) {
            $form.find('.js-step-nav-item').eq(i).addClass('done')
        }
        if ($index > 0) {
            $form.find('.js-step-prev').removeClass('hide')
        }
        if ($index == $counts - 1) {
            $form.find('.js-steps-nav').addClass('hide');
            $form.find('.js-quiz-buttons').addClass('hide')
        }
    })

    $('.js-step-prev').on('click', function () {
        var $form = $(this).closest('.js-tab'),
            $index = $form.find('.js-step').filter('.active').index() - 1,
            $counts = $form.find('.js-step').length
        $form.find('.js-step').filter('.active').prev('.js-step').addClass('active').siblings('.js-step').removeClass('active');
        $form.find('.js-step-nav-item').eq($index).addClass('active').siblings('.js-step-nav-item').removeClass('active');
        $form.find('.js-step-nav-item').removeClass('done')
        for (var i = 0; i < $index; i++) {
            $form.find('.js-step-nav-item').eq(i).addClass('done')
        }
        if ($index == 0) {
            $form.find('.js-step-prev').addClass('hide')
        } else {
            $form.find('.js-step-prev').removeClass('hide')
        }
    });



    // validation forms
    var acceptInput = $('.acceptance input');
    acceptInput.addClass('acceptance').prependTo('.checkbox-element');

    $('.checkbox-quiz').each(function () {
        $(this).find('.quiz-input input').prependTo($(this));
    });

    $('.quiz-input input').prependTo('.checkbox-quiz');

    if ($('#callback .wpcf7-form').length) {

        $('#callback .wpcf7-form').validate({
            rules: {
                clientName: {
                    required: true,
                    minlength: 2
                },
                clientPhone: {
                    required: true,
                    minlenghtphone: true
                },
                acceptance: {
                    required: true
                }
            },
            messages: {
                clientName: {
                    required: 'Поле должно быть заполнено',
                    minlength: 'Минимум 2 символа'
                },
                clientPhone: {
                    required: 'Поле должно быть заполнено'
                },
                acceptance: {
                    required: "Необходимо согласие"
                }
            },

            submitHandler: function (form) {
                $.fancybox.close();
                setTimeout(function () {
                    $.fancybox.open($('#successCallback'), {
                        btnTpl: {
                            smallBtn: '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small"><i class="icon-close"></i></button>'
                        }
                    });
                }, 10);
            }

        });
    }

    $('.form-order-inline .wpcf7-form ,#order .wpcf7-form').each(function () {
        var $el = $(this);

        $el.validate({
            rules: {
                nameOrder: {
                    required: true,
                    minlength: 2
                },
                telOrder: {
                    required: true,
                    minlenghtphone: true
                },
                mailOrder: {
                    required: true,
                    email: true
                },

                servicesOrder: {
                    required: true
                },
                acceptance: {
                    required: true
                },
                dateOrder: {
                    required: true
                }
            },
            messages: {
                nameOrder: {
                    required: 'Поле должно быть заполнено',
                    minlength: 'Минимум 2 символа'
                },
                telOrder: {
                    required: 'Поле должно быть заполнено'
                },
                mailOrder: {
                    required: 'Поле должно быть заполнено',
                    email: 'ВВедите корректный email'
                },

                servicesOrder: {
                    required: 'Выберите услугу'
                },
                acceptance: {
                    required: "Необходимо согласие"
                },
                dateOrder: {
                    required: 'Выберите дату'
                }
            },

            validHandler: function () {

                setTimeout(function () {
                    $('.js-styled').trigger('refresh');
                }, 1)

            },

            submitHandler: function (form) {
                $.fancybox.close();
                setTimeout(function () {
                    $.fancybox.open($('#successCallback'), {
                        btnTpl: {
                            smallBtn: '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small"><i class="icon-close"></i></button>'
                        }
                    });
                }, 10);
            }

        });
    });

    $('.js-form-quiz, .quiz__form .wpcf7-form').each(function () {
        var $el = $(this);

        $el.validate({
            rules: {
                nameQuiz: {
                    required: true,
                    minlength: 2
                },
                mailQuiz: {
                    required: true,
                    email: true
                },
                acceptance: {
                    required: true
                },

            },
            messages: {
                nameQuiz: {
                    required: 'Поле должно быть заполнено',
                    minlength: 'Минимум 2 символа'
                },

                mailQuiz: {
                    required: 'Поле должно быть заполнено',
                    email: 'ВВедите корректный email'
                },
                acceptance: {
                    required: "Необходимо согласие"
                }
            },

            validHandler: function () {

                setTimeout(function () {
                    $('.js-styled').trigger('refresh');
                }, 1)

            },
            submitHandler: function (form) {
                $.fancybox.close();
                location.reload();
                setTimeout(function () {
                    $.fancybox.open($('#successCallback'), {
                        btnTpl: {
                            smallBtn: '<button id="quiz-reload" type="button" data-fancybox-close class="fancybox-button fancybox-close-small"><i class="icon-close"></i></button>'
                        }
                    });
                }, 10);
            }

        });
    });

    if ($('#wpcf7-f329-o1 .wpcf7-form').length) {
        $('#wpcf7-f329-o1 .wpcf7-form').validate({
            rules: {
                nameFeedback: {
                    required: true,
                    minlength: 2
                },
                telFeedback: {
                    required: true,
                    minlenghtphone: true
                },
                mailFeedback: {
                    required: true,
                    email: true
                },

                commentFeedback: {
                    required: true
                },
                acceptance: {
                    required: true
                }
            },
            messages: {
                nameFeedback: {
                    required: 'Поле должно быть заполнено',
                    minlength: 'Минимум 2 символа'
                },
                telFeedback: {
                    required: 'Поле должно быть заполнено'
                },
                mailFeedback: {
                    required: 'Поле должно быть заполнено',
                    email: 'ВВедите корректный email'
                },

                commentFeedback: {
                    required: 'Поле должно быть заполнено',
                    minlength: 'Минимум 2 символа'
                },
                acceptance: {
                    required: "Необходимо согласие"
                }
            },

            validHandler: function () {

                setTimeout(function () {
                    $('.js-styled').trigger('refresh');
                }, 1)

            },

            submitHandler: function (form) {
                $.fancybox.close();
                setTimeout(function () {
                    $.fancybox.open($('#successCallback'), {
                        btnTpl: {
                            smallBtn: '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small"><i class="icon-close"></i></button>'
                        }
                    });
                }, 10);
            }

        });
    }


    if ($('.js-mask-tel').length) {
        $.validator.addMethod("minlenghtphone", function (value, element) {
            return value.replace(/\D+/g, '').length > 10;
        },
            "Телефон должен быть минимум 11 символов");


    }
    // validate form

    $('.specialist--nav .tabs-nav .tabs-nav__link:first-child, .specialist-tab .tab:first-child').addClass('active');

    $('.equipment-nav .js-tab-link:first-child, .equipment-tab .tab:first-child').addClass('active');

    $('.reviews-slides .tabs-nav__link:first-child, .review-tab .tab:first-child').addClass('active');

    $('.price-tabs .tab:first-child, .tabs-nav .tabs-nav__link:first-child').addClass('active');

    // $('#services').load('services.html');

    // setTimeout(function() {
    // },3000)

    if (/Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor)) {
        $body.addClass('safari')
    }





    var codeMaskInput = function () {
        //mask tel
        if ($('.js-mask-tel').length > 0) {
            $('.js-mask-tel').inputmask("+8(999) 999 99 99");
        }
    }
    var codeFancybox = function () {
        if ($('.js-fancybox').length) {

            $(".js-fancybox").fancybox({
                padding: 0,
                margin: 20,
                touch: false,
                loop: true,
                animationEffect: 'zoom',
                transitionEffect: "zoom-in-out",
                speed: 350,
                transitionDuration: 300,
                smallBtn: true,
                backFocus: false,
                btnTpl: {
                    smallBtn: '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small"><i class="icon-close"></i></button>'
                },
                afterShow: function () {

                    $('html').css('margin-top', -offset);
                },
                afterLoad: function () {
                    lockScroll();
                },

                afterClose: function () {

                    unlockScroll();
                }
            })
        }
    }

    var codeScrollbar = function () {

        if ($('.js-scroll').length) {
            instance = $('.js-scroll').overlayScrollbars({
                overflowBehavior: {
                    x: "visible",
                    y: "scroll"
                },

                callbacks: {
                    onScroll: function () {

                    }
                }
            }).overlayScrollbars();
        }
    }
    var onWowCallback = function () {
        if ($('.wow').length) {
            var wow = new WOW({
                boxClass: 'wow',
                animateClass: 'animated',

                mobile: false,
                live: true,
                scrollContainer: null,
                callback: function (box) {
                }
            })
            wow.init();
        }
    }
    var onLazyCallback = function () {
        if ($('.js-lazy').length) {
            var lazyLoadInstance = new LazyLoad({
                elements_selector: ".js-lazy"
            });

            if (lazyLoadInstance) {
                lazyLoadInstance.update();
            }
        }

    }

    if ($('.js-lazy').length) {
        console.log(8)
        loadJS('/wp-content/themes/reverse/js/components/jquery.lazy.min.js', onLazyCallback, document.body);

    }
    if ($('.js-scroll').length) {
        loadJS('/wp-content/themes/reverse/js/components/jquery.overlayScrollbars.js', codeScrollbar, document.body);
    }
    if ($('.js-fancybox').length) {
        loadJS('/wp-content/themes/reverse/js/components/jquery.fancybox.min.js', codeFancybox, document.body);
    }
    if ($('.js-mask-tel').length > 0) {
        loadJS('/wp-content/themes/reverse/js/components/maskedinput.js', codeMaskInput, document.body);
    }


    if ($('.wow').length) {
        loadJS('/wp-content/themes/reverse/js/components/wow.min.js', onWowCallback, document.body);

    }
});

$('.tabs-nav.equipment-nav .tabs-nav__link:first-child').addClass('active');
$('.quiz-tab-wrap .tab:first-child').addClass('active');
$('.steps-quiz-nav .steps-quiz-nav__item:first-child').addClass('active');
$('.contacts-nav .tabs-nav__link:first-child').addClass('active');