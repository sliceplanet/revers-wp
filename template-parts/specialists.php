<div class="section-specialist">
    <div class="wrapper">
        <div class="h2-decor wow fadeInRigthLeft"><?php echo get_field('specailists_title', 'option'); ?></div>
        <div class="specialist js-tabs-wrap">

            <div class="specialist--nav scroll-unvisible js-scroll-horizontal wow fadeInRigthShort"  data-wow-delay="0.3s">

                <div class="tabs-nav specialist-nav js-horizontal">
                    <?php foreach ( get_field('specialists_list',  'option') as $item_spec ) { ?>
                        <div class="tabs-nav__link js-tab-link js-slide" data-href="#<?= $item_spec['specialists_item_ID'] ?>"><?= $item_spec['specialists_item_title_nav'] ?></div>
                    <?php } ?>
                </div>

            </div>

            <div class="tabs-wrap specialist-tab wow fadeInUpShort" data-wow-delay="0.6s">
                <?php foreach ( get_field('specialists_list',  'option') as $item_spec ) { ?>
                    <div class="tab js-tab" id="<?= $item_spec['specialists_item_ID'] ?>">
                        <div class="specialist-slider sliders-arrow js-slider-specialist">
                            <?php foreach ( $item_spec['specialists_item_doctors_list'] as $doc ) { ?>
                                <div class="specialist-slider__item">
                                    <div class="specialist-slider__card" style="background: radial-gradient(100% 143.45% at 0% 52.01%, #fffffe 0%, #fff4e0 45.35%);">
                                        <div class="specialist-slider__head">
                                            <div class="specialist-slider__title"><?= $doc['specialists_item_doctors_item_name'] ?></div>
                                            <div class="specialist-slider__position"><?= $doc['specialists_item_doctors_item_position'] ?></div>
                                            <div class="specialist-slider__experience"><?= $doc['specialists_item_doctors_item_exp'] ?></div>
                                        </div>
                                        <div class="specialist-slider__img">
                                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
                                                 data-src="<?= $doc['specialists_item_doctors_item_photo'] ?>" alt="" class="js-img" /> <!-- 224*329-->
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="specialist-decor wow fadeInUpShort" data-wow-delay="0.6s">
            <svg width="1120" height="313" viewBox="0 0 1120 313" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path opacity="0.5" d="M0 156.864C0 65.4545 78.1768 -6.40383 169.263 1.28156L1009.52 72.178C1071.98 77.4485 1120 129.688 1120 192.375C1120 258.995 1065.99 313 999.375 313H156.136C69.9043 313 0 243.096 0 156.864Z" fill="#E6E7EC" />
            </svg>
        </div>
    </div>
</div>