<?php get_header(); ?>

<main class="content">
    <section class="head-inner-2">
        <div class="wrapper">

            <!--    Breadcrumbs -->
            <?php if ( function_exists( 'breadcrumbs' ) ) breadcrumbs(); ?>
            <!--    End Breadcrumbs -->

            <div class="content">
                <?php while (have_posts()) : the_post(); ?>
                    <?php the_content(); ?>
                <?php endwhile; ?>
            </div>

        </div>
    </section>
</main>

<?php get_footer(); ?>
