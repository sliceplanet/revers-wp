<?php
/*
 * Template name: Статья
 */
?>

<?php get_header(); ?>

<main class="content">

    <div class="head-inner-2 head-inner-2_contacts">
        <div class="wrapper">
            <!--    Breadcrumbs -->
            <?php if ( function_exists( 'breadcrumbs' ) ) breadcrumbs(); ?>
            <!--    End Breadcrumbs -->
        </div>
    </div>

    <article class="article-wrap">
        <div class="wrapper article">
            <h1 class="h2-decor fadeInLeftShort wow"><?php the_title() ?></h1>
            <?php
            $text_blocks = get_field('article_txt_blocks');
            foreach ( $text_blocks as $item ) {
            ?>
            <div class="article-el fadeInUpShort wow" data-wow-delay="0.2s">
                <?php if ( !empty ( $item['article_txt_block_img'] ) ) { ?>
                    <img src="<?= $item['article_txt_block_img'] ?>"
                         alt=""
                         style="float:<?= $item['article_txt_block_img_side'] ?>;"> <!-- или вместо align="right" поставить style="float:right;" -->
                <?php
                }
                ?>
                <h2><?= $item['article_txt_block_subtitle'] ?></h2>
                <p><?= $item['article_txt_block_txt'] ?></p>
            </div>
            <?php } ?>

            <div class="btns-share fadeInLeftShort wow">
                <?php echo do_shortcode('[Sassy_Social_Share]'); ?>
            </div>

        </div>
    </article>

    <?php
    $gallery = get_field('article_gallery_slider');
    if ( !empty ($gallery) ) { ?>
    <div class="wrapper photogallery-section">
        <div class="photogallery">
            <h2 class="h2 fadeInLeftShort wow">фотогалерея</h2>
            <div class="photogallery-slider sliders-arrow js-slider-4 fadeInUpShort wow" data-wow-delay="0.2s">
                <?php foreach ( $gallery as $i ) { ?>
                    <div class="photogallery__item"><div class="photogallery__el js-fancybox js-bg" data-fancybox="photogallery" data-src="<?= $i['article_gallery_img'] ?>" data-preload="<?= $i['article_gallery_img'] ?>"></div></div>
                <?php } ?>
            </div>
        </div>
        <div class="specialist-decor wow fadeInUpShort" data-wow-delay="0.6s">
            <svg width="1120" height="313" viewBox="0 0 1120 313" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path opacity="0.5" d="M0 156.864C0 65.4545 78.1768 -6.40383 169.263 1.28156L1009.52 72.178C1071.98 77.4485 1120 129.688 1120 192.375C1120 258.995 1065.99 313 999.375 313H156.136C69.9043 313 0 243.096 0 156.864Z" fill="#E6E7EC" />
            </svg>
        </div>
    </div>
    <?php } ?>

    <?php
        $services = get_field('article_services');
        if ( !empty ( $services ) ) {
    ?>
    <div class="services-main services-main_inner services-slider-wrap fadeInUpShort wow">
        <div class="wrapper">
            <div class="services-slider js-slider-5">
                <?php foreach ( $services as $service ) { ?>
                    <?php
                    $obj   = $service['article_service_item'];
                    $price = get_field('post_service_price', $obj -> ID );
                    ?>
                    <div class="services-slider__item">
                        <a href="<?php echo get_permalink( $obj -> ID ); ?>" class="services-main__card">
                            <div class="services-main__picture">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
                                     data-src="<?php echo get_the_post_thumbnail_url( $obj ); ?>"
                                     alt=""
                                     class="js-img" />
                            </div>
                            <div class="services-main__head">
                                <span><?= $obj -> post_title ?></span>
                                <div class="services-main__price"><?= $price ?></div>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php } ?>

</main>

<?php get_footer(); ?>

<script>
    var list = $('.heateor_sss_sharing_ul');
    var icon = '<li class="custom-sharing-item heateorSssSharingRound"><a href="#" class="btns-share__link" target="_blank"><i class="icon-more"></i></a></li>';
    list.prepend(icon);
</script>