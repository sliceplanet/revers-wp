<?php
/*
Template Name: Страница Контактов
*/
?>

<?php get_header(); ?>

    <main class="content">
    <section class="head-inner-2 head-inner-2_contacts">

        <div class="wrapper">

            <!--    Breadcrumbs -->
            <?php if ( function_exists( 'breadcrumbs' ) ) breadcrumbs(); ?>
            <!--    End Breadcrumbs -->

            <h1 class="h2-decor wow fadeInLeftShort"><?php the_title(); ?></h1>

        </div>

        <div class="head-decor"><?php echo get_field('p_contact_decor'); ?></div>
    </section>
    <section class="main-contacts">
        <div class="wrapper">
            <div class="contacts">
                <div class="contacts__text wow fadeInLeftShort" data-wow-delay="0.8s">
                    <h2 class="h2"><?php echo get_field('p_contact_textarea_title'); ?></h2>
                    <p><?php echo get_field('p_contact_textarea_txt'); ?></p>
                </div>

                <div class="contacts__form wow fadeInUpShort" data-wow-delay="0.4s">
                    <div class="popup-title">обратная связь</div>
                    <?php echo do_shortcode('[contact-form-7 id="329" title="Страница контакты"]'); ?>
<!--                    <form id="feedback">-->
<!--                        <div class="box-field">-->
<!--                            <div class="box-field__label">Имя</div>-->
<!--                            <div class="box-field__input">-->
<!--                                <input type="text" class="form-control form-control_lg" name="nameFeedback" placeholder="Ваше имя" />-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="box-field">-->
<!--                            <div class="box-field__label">Телефон</div>-->
<!--                            <div class="box-field__input">-->
<!--                                <input type="text" class="form-control form-control_lg js-mask-tel" name="telFeedback" placeholder="Ваш номер телефона" />-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="box-field">-->
<!--                            <div class="box-field__label">Email</div>-->
<!--                            <div class="box-field__input">-->
<!--                                <input type="text" class="form-control form-control_lg" placeholder="Ваша почта" name="mailFeedback" />-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="box-field">-->
<!--                            <div class="box-field__label">Комментарий</div>-->
<!--                            <div class="box-field__input">-->
<!--                                <textarea class="form-control" placeholder="Ваш комментарий" name="commentFeedback"></textarea>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="box-field box-field_checkbox">-->
<!--                            <label class="checkbox-element">-->
<!--                                <input type="checkbox" name="acceptRules3">-->
<!--                                <span class="check-label">-->
<!--                                        <span class="check"></span>-->
<!--                                        </span>-->
<!--                            </label><a href="#">Политика конфиденциальности</a>-->
<!--                        </div>-->
<!--                        <div class="popup-button">-->
<!--                            <input type="submit" class="btn btn_popup" value="записаться" />-->
<!--                        </div>-->
<!--                    </form>-->

                </div>

            </div>
            <div class="decor-left decor-contacts"></div>
        </div>
    </section>
</main>

<?php get_footer(); ?>
