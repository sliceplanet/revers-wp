<?php get_header(); ?>

<main class="content">
    <section class="head-inner-2 head-inner-2_contacts">
        <div class="wrapper">
            <div class="text-center">
                <h1>404</h1>
                <p>Такой старницы не существует. <br />Пожалуйста вернитесь на предыдущую страницу или воспользуйтесь меню сайта.</p>
            </div>
        </div>
    </section>
</main>

<?php get_footer(); ?>