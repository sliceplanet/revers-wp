<?php
/*
Template Name: Страница Процедур
*/
?>

<style>
    .wpcf7-form-control-wrap.result { display: none; }
    .wpProQuiz_content .wpProQuiz_reviewDiv .wpProQuiz_button2,
    .wpProQuiz_content .wpProQuiz_reviewDiv .wpProQuiz_reviewLegend { display: none !important; }
    .wpProQuiz_quiz .wpProQuiz_question_text { margin-bottom: 3rem; font-size: 3.4rem; line-height: 1.2; letter-spacing: 0.085em; text-transform: uppercase; color: rgba(39, 42, 53, 0.8); }
</style>

<?php get_header(); ?>

<main class="content">
    <section class="head-inner-2 head-inner-2_contacts">
        <div class="wrapper">

            <!--    Breadcrumbs -->
            <?php if ( function_exists( 'breadcrumbs' ) ) breadcrumbs(); ?>
            <!--    End Breadcrumbs -->

            <h1 class="h2-decor wow fadeInLeftShort"><?php the_title(); ?></h1>

            <div class="section-text section-text_quiz wow fadeInUpShort" data-wow-delay="0.3s">
                <p><?php echo get_field('p_procedure_desc'); ?></p>
            </div>

        </div>
    </section>

    <div class="main-quiz">
        <div class="wrapper">
            <div class="quiz  js-tabs-wrap">
                <div class="equipment-nav-wrap quiz__nav scroll-unvisible js-scroll js-scroll-horizontal  wow fadeInLeftShort" data-wow-delay="0.6s">
                    <div class="tabs-nav equipment-nav js-horizontal">
                        <?php $i = 1; foreach ( get_field('quieze_list') as $quiz ) { ?>
                            <div class="tabs-nav__link js-tab-link js-slide" data-href="#quiz<?= $i++; ?>"><?= $quiz['quieze_item_tab_name'] ?></div>
                        <?php } ?>
                    </div>
                </div>

                <div class="quiz__form">
                    <div class="tabs-wrap quiz-tab wow fadeInUpShort" data-wow-delay="0.3s">
                        <div class="quiz-tab-wrap">
                            <?php $j = 1; foreach ( get_field('quieze_list') as $quiz ) { ?>
                                <div class="tab js-tab" id="quiz<?= $j++; ?>">
                                    <!--                                    <div class="steps-quiz-nav js-steps-nav">-->
                                    <!--                                        --><?php //foreach ( $quiz['quieze_item_steps'] as $step ) { ?>
                                    <!--                                            <div class="steps-quiz-nav__item js-step-nav-item">-->
                                    <!--                                                <div class="steps-quiz-nav__count">--><?//= $step['quieze_item_step_item'] ?><!--</div>-->
                                    <!--                                                <div class="steps-quiz-nav__el"></div>-->
                                    <!--                                            </div>-->
                                    <!--                                        --><?php //} ?>
                                    <!--                                    </div>-->
                                    <?= $quiz['quieze_item_tab_form'] ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="decor-right decor-quiz js-bg" data-preload="<?php bloginfo('template_url') ?>/img/examples/quiz-bg.jpg"></div>
                </div>
            </div>
        </div>
    </div>
</main>


<?php get_footer(); ?>


<script>

    $items = $('.wpProQuiz_resultsItem');
    console.log($items.length);
    $items.each(function () {

        $formRes = $(this).find('.wpcf7 .result .wpcf-res-field');
        $res = $(this).find('.quiz-res p').text();

        $formRes.attr('value', $res);
    });
    
</script>